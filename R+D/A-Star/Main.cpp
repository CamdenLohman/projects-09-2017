// Std. Includes
#include <string>
#include <vector>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"

// GLM Mathemtics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other Libs
#include <SOIL.h>

#define _USE_MATH_DEFINES // for C++  
#include <math.h> 

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

float main_xp, main_yp, main_zp;

GLfloat main_Yaw = 0.0f;
GLfloat main_Pitch = 0.0f;

const int grid_width = 20;
const int grid_height = 20;

/*int grid_array[grid_width][grid_height] = {
		{ 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 1, 1, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 0, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 1, 1, 1 },
		{ 1, 0, 1, 0, 1, 0, 0, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 1, 1, 1, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };
		*/

/*int grid_array2[grid_width][grid_height] = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 0, 1 },
		{ 1, 0, 0, 0, 1, 0, 1, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 } };
		*/

int grid_array3[grid_width][grid_height] = {
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1 },
		{ 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1 },
		{ 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1 },
		{ 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1 },
		{ 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };


class Object
{
public:
	//The dimensions of the dot
	float OBJECT_WIDTH;
	float OBJECT_HEIGHT;
	float OBJECT_DEPTH;

	//Maximum axis velocity of the dot
	float OBJECT_VEL = 0;

	//Initializes the variables
	Object(float x, float y, float z, float sizex, float sizey, float sizez, int type, int grid_x, int grid_y, int id);

	int mPosXPub = 0;
	int mPosYPub = 0;
	int mPosZPub = 0;

	int Type;

	int fcost, gcost, hcost;

	int Grid_x, Grid_y;

	int id;

	int parent;

	float mPosX = mPosXPub;
	float mPosY = mPosYPub;
	float mPosZ = mPosZPub;
	float mVelX, mVelY, mVelZ;
	float Sizex, Sizey, Sizez;

	glm::vec3 Object_Position;
	glm::mat4 Object_model;

	//Shows the dot on the screen relative to the camera
	void render(int camX, int camY);

	//Position accessors
	int getPosX();
	int getPosY();
	int getPosZ();

	glm::vec3 getPosition();

	int getType();

	void setType(int input);

	int getGridX();
	int getGridY();

	vector<int>Connected;

	void addConnection(int input);

	vector<int> getConnections();

	int getID();

	void calcfcost();

	int getFCost();
	int getGCost();
	int getHCost();

	void setGCost(int input);
	void setHCost(int input);

	void setFCost(int input);

	void setParent(int input);
	int getParent();

	//SDL_Rect mCollider;

private:



	//Dot's collision box

};


#pragma region Object
Object::Object(float x, float y, float z, float sizex, float sizey, float sizez, int type, int grid_x, int grid_y, int input_id)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;
	mPosZ = z;

	Object_Position = glm::vec3(mPosX, mPosY, mPosZ);

	Sizex = sizex;
	Sizey = sizey;
	Sizez = sizez;

	Type = type;

	Grid_x = grid_x;
	Grid_y = grid_y;

	id = input_id;

	//Set collision box dimension
	//mCollider.w = DOT_WIDTH;
	//mCollider.h = DOT_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
}

//Current animation frame
int frame = 0;

void Object::render(int camX, int camY)
{
	/*
	if (moving == true)
	{
	//Show the dot relative to the camera
	gDotTexture.render(mPosX - camX, mPosY - camY, currentClip, NULL, NULL, flipType);
	}
	else
	{
	gDotTexture.render(mPosX - camX, mPosY - camY, gSpriteClips, NULL, NULL, flipType);
	}
	*/
}

int Object::getPosX()
{
	return mPosX;
}

int Object::getPosY()
{
	return mPosY;
}

glm::vec3 Object::getPosition()
{
	return Object_Position;
}
int Object::getType()
{
	return Type;
}

void Object::setType(int input){
	Type = input;
}

int Object::getGridX()
{
	return Grid_x;
}

int Object::getGridY()
{
	return Grid_y;
}

void Object::addConnection(int input)
{
	Connected.push_back(input);
}

vector<int> Object::getConnections()
{
	return Connected;
}

int Object::getID()
{
	return id;
}

void Object::calcfcost(){
	fcost = gcost + hcost;
}

int Object::getFCost(){
	return fcost;
}

int Object::getGCost(){
	return gcost;
}

int Object::getHCost(){
	return hcost;
}

void Object::setGCost(int input){
	gcost = input;
}

void Object::setHCost(int input){
	hcost = input;
}

void Object::setFCost(int input){
	fcost = input;
}

void Object::setParent(int input){
	parent = input;
}

int Object::getParent(){
	return parent;
}
#pragma endregion


vector<Object>Objects;
vector<Object>Final_Path;

void vector_clear()
{
	Objects.clear();
}

void level_loader()
{
	vector_clear();
	for (unsigned int i = 0; i < grid_width; i++)
	{
		for (unsigned int j = 0; j < grid_height; j++)
		{

			int gridtype;

			gridtype = grid_array3[i][j];

			bool CC = false;

			if (gridtype == 0)
			{
				CC = true;
				Object cb_Object(0.4 * j, 0, 0.4 * i, 0.4, 0.4, 0.4, gridtype, j, i, ((i * grid_width) + j));
				Objects.push_back(cb_Object);
			}
			if (gridtype == 1)
			{
				CC = true;
				Object cb_Object(0.4 * j, 0, 0.4 * i, 0.4, 0.4, 0.4, gridtype, j, i, ((i * grid_width) + j));
				Objects.push_back(cb_Object);
			}
		}

	}
}

void find_neighbours(int input_num)
{
	if (Objects[input_num].getType() == 0){
		if (Objects[input_num].getGridX() != 0) // if not all the way left
		{
			if (Objects[input_num].getGridY() != 0) // if not all the way up
			{
				if (Objects[input_num - (grid_width + 1)].getType() == 0){ // if UL.type == 0
					//Objects[input_num].addConnection(input_num - (grid_width + 1)); // add UL
				}
			}
			if (Objects[input_num - 1].getType() == 0){ // if L.type == 0
				Objects[input_num].addConnection(input_num - 1); // add L
			}

		}
		if (Objects[input_num].getGridY() != 0) // if not all the way up
		{
			if (Objects[input_num - grid_width].getType() == 0){ // if U.type == 0
				Objects[input_num].addConnection(input_num - grid_width); // add U
			}
			if (Objects[input_num].getGridX() != (grid_width - 1)) // if not all the way right
			{
				if (Objects[input_num - (grid_width - 1)].getType() == 0){ // if UR.type == 0
					//Objects[input_num].addConnection(input_num - (grid_width - 1)); // add UR
				}
			}
		}
		if (Objects[input_num].getGridX() != (grid_width - 1)) // if not all the way right
		{

			if (Objects[input_num + 1].getType() == 0){ // if R.type == 0
				Objects[input_num].addConnection(input_num + 1); // add R
			}
			if (Objects[input_num].getGridY() != (grid_height - 1)) // if not all the way down
			{
				if (Objects[input_num + (grid_width + 1)].getType() == 0){ // if DR.type == 0
					//Objects[input_num].addConnection(input_num + (grid_width + 1)); // add DR
				}
			}
		}

		if (Objects[input_num].getGridY() != (grid_height - 1)) // if not all the way down
		{
			if (Objects[input_num + grid_width].getType() == 0){ // if D.type == 0
				Objects[input_num].addConnection(input_num + grid_width); // add D
			}
			if (Objects[input_num].getGridX() != 0) // if not all the way left
			{
				if (Objects[input_num + (grid_width - 1)].getType() == 0){ // if DL.type == 0
					//Objects[input_num].addConnection(input_num + (grid_width - 1)); // add DL
				}
			}
		}

	}
}


void RetracePath(Object start, Object End){
	vector<Object> path;
	vector<Object> path2;
	Object currentObject = End;

	while (currentObject.getID() != start.getID()){
		for (int i = 0; i < Objects.size(); i++){
			if (Objects.at(i).getID() == currentObject.getParent()){
				path.push_back(currentObject);

				if (currentObject.getParent() != start.getID()){
					currentObject = Objects.at(Objects.at(currentObject.getID()).getParent());
				}
				else {
					currentObject = Objects.at(start.getID());
				}

				i = Objects.size();
			}
		}
	}

	for (int i = path.size(); i > 0; i--){
		path2.push_back(path.at(i - 1));
	}

	for (int i = 0; i < path2.size(); i++){
		Final_Path.push_back(path2.at(i));
	}

}

int GetDistance(Object objecta, Object objectb){
	int dstX = (objecta.getGridX() - objectb.getGridX());
	int dstY = (objecta.getGridY() - objectb.getGridY());
	int answer;

	if (dstX < 0){
		dstX -= (dstX * 2);
	}

	if (dstY < 0){
		dstY -= (dstY * 2);
	}

	if (dstX > dstY)
	{
		answer = ((14 * dstY) + (10 * (dstX - dstY)));
	}
	else{
		answer = ((14 * dstX) + (10 * (dstY - dstX)));
	}
	return answer;


}


void find_path(Object start_Num, Object End_Num){
	vector<Object>OpenSet;
	vector<Object>ClosedSet;


	OpenSet.push_back(start_Num);

	Object currentObject = OpenSet[0];

	OpenSet[0].setFCost(GetDistance(start_Num, End_Num));

	while (OpenSet.size() > 0)
	{
		/*for (int i = 0; i < OpenSet.size(); i++){
			if (OpenSet[i].getFCost() < currentObject.getFCost())
			{
				currentObject = Objects.at(OpenSet[i].getID());
				i = 0;
			}
		}*/

		if (currentObject.getID() == End_Num.getID()){
			//finish here?
			ClosedSet.push_back(currentObject);
			cout << "Hello" << endl;
			for (int i = 0; i < ClosedSet.size(); i++){
				cout << i << " position id : " << ClosedSet.at(i).getID() << endl;
			}

			for (int i = 0; i < currentObject.getConnections().size(); i++){
				for (int j = 0; j < ClosedSet.size(); j++){
					if (Objects.at(currentObject.getConnections().at(i)).getID() == ClosedSet.at(j).getID()){
						Objects.at(End_Num.getID()).setParent(ClosedSet.at(j).getID());
						j = ClosedSet.size();
						i = currentObject.getConnections().size();
					}
				}
			}
			RetracePath(Objects.at(start_Num.getID()), Objects.at(End_Num.getID()));
			OpenSet.clear();
		}
		else {
			for (int i = 0; i < currentObject.getConnections().size(); i++){
				Objects.at(currentObject.getConnections().at(i)).setGCost(currentObject.getGCost() + (GetDistance(currentObject, Objects.at(currentObject.getConnections().at(i)))));
				Objects.at(i).calcfcost();

				for (int j = 0; j < ClosedSet.size(); j++){
					if (ClosedSet.at(j).getID() == currentObject.getConnections().at(i)){
						if (Objects.at(currentObject.getConnections().at(i)).getGCost() <= currentObject.getFCost()){
							//Do Nothing
						}
						else{
							int OpenSetChecker = 0;

							for (int k = OpenSet.size() - 1; k > 0; k--){
								if (OpenSet.at(k).getFCost() <= Objects.at(currentObject.getConnections().at(i)).getFCost()){
									OpenSetChecker = k;
									k = 0;
								}
							}

							bool checker = false;
							for (int k = 0; k < OpenSet.size(); k++){
								if (OpenSet.at(k).getID() == Objects.at(currentObject.getConnections().at(i)).getID()){
									checker = true;
									k = OpenSet.size();
								}
							}
							for (int k = 0; k < ClosedSet.size(); k++){
								if (ClosedSet.at(k).getID() == Objects.at(currentObject.getConnections().at(i)).getID()){
									checker = true;
									k = ClosedSet.size();
								}
							}
							if (checker == false){
								OpenSet.insert(OpenSet.begin() + OpenSetChecker, Objects.at(currentObject.getConnections().at(i)));
								ClosedSet.erase(ClosedSet.begin() + j);
								Objects.at(currentObject.getConnections().at(i)).setHCost(currentObject.getFCost() - currentObject.getGCost());
								Objects.at(currentObject.getConnections().at(i)).calcfcost();
							}
							checker = false;

							
						}
					}
					
				}
				for (int j = 0; j < OpenSet.size(); j++){
					if (OpenSet.at(j).getID() == currentObject.getID()){
						if (OpenSet.at(j).getID() == currentObject.getConnections().at(i)){
							if (Objects.at(currentObject.getConnections().at(i)).getGCost() <= currentObject.getFCost()){
								//Do Nothing
							}
							else{
								Objects.at(currentObject.getConnections().at(i)).setHCost(currentObject.getFCost() - currentObject.getGCost());
								Objects.at(currentObject.getConnections().at(i)).calcfcost();
							}
						}
						else {
							bool checker = false;
							for (int k = 0; k < OpenSet.size(); k++){
								if (OpenSet.at(k).getID() == Objects.at(currentObject.getConnections().at(i)).getID()){
									checker = true;
									k = OpenSet.size();
								}
							}
							for (int k = 0; k < ClosedSet.size(); k++){
								if (ClosedSet.at(k).getID() == Objects.at(currentObject.getConnections().at(i)).getID()){
									checker = true;
									k = ClosedSet.size();
								}
							}
							if (checker == false){
								int OpenSetChecker = 0;

								for (int k = OpenSet.size() - 1; k > 0; k--){
									if (OpenSet.at(k).getFCost() <= Objects.at(currentObject.getConnections().at(i)).getFCost()){
										OpenSetChecker = k;
										k = 0;
									}
								}
								OpenSet.insert(OpenSet.begin() + OpenSetChecker, Objects.at(currentObject.getConnections().at(i)));
								Objects.at(currentObject.getConnections().at(i)).setParent(currentObject.getID());
							}
							checker = false;
							
						}
					}
				}
			}
			for (int i = 0; i < OpenSet.size(); i++){
				if (OpenSet.at(i).getID() == currentObject.getID()){
					OpenSet.erase(OpenSet.begin() + i);
					i = OpenSet.size();
				}
			}

			bool checker = false;
			for (int k = 0; k < ClosedSet.size(); k++){
				
				if (ClosedSet.at(k).getID() == currentObject.getID()){
					checker = true;
					k = ClosedSet.size();
				}
			}
			if (checker == false){
				ClosedSet.push_back(Objects.at(currentObject.getID()));
			}
			checker = false;
			
			
			if (currentObject.getID() != End_Num.getID()){
				currentObject = OpenSet[0];
			}
		}

	}
}

int main()
{
	// Init GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "A-Star Maze Test", nullptr, nullptr); // Windowed
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW to setup the OpenGL Function pointers
	glewExperimental = GL_TRUE;
	glewInit();

	// Define the viewport dimensions
	glViewport(0, 0, screenWidth, screenHeight);

	// Setup some OpenGL options
	glEnable(GL_DEPTH_TEST);

	// Setup and compile our shaders
	Shader shader("nano.vs", "nano.frag");

	// Load models
	//Model ourModel("Mesh_Files/nanosuit.obj");
	Model ourModel("Blender Output/test.obj");
	Model ourModel2("Blender Output/floor.obj");
	Model GridModel1("Blender Output/test4.obj");
	Model GridModel2("Blender Output/test3.obj");
	Model GridModel3("Blender Output/test.obj");


	// Draw in wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	float main_x = 0.0;
	float main_y = 0.0;
	float main_z = 0.0;

	float main2_x = 0.0;
	float main2_y = -0.5;
	float main2_z = 0.0;

	glm::vec3 main_Position;
	glm::vec3 main_Front = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 main_Up;
	glm::vec3 main_Right;
	glm::vec3 world_Up = glm::vec3(0.0f, 1.0f, 0.0f);

	int temp_int;

	GLfloat velocity;
	GLfloat SPEED = 3.0f;

	level_loader();

	for (int i = 0; i < Objects.size(); i++)
	{
		find_neighbours(i);
	}

	// targets here

	for (int i = 0; i < Objects.size(); i++)
	{
		Objects.at(i).setGCost(GetDistance(Objects.at(i), Objects.at(381)));
		Objects.at(i).setHCost(GetDistance(Objects.at(i), Objects.at(20)));

		Objects.at(i).calcfcost();
	}

	find_path(Objects.at(381), Objects.at(20));

	for (int i = 0; i < Objects.size(); i++)
	{
		for (int j = 0; j < Final_Path.size(); j++){
			if (Objects.at(i).getID() == Final_Path.at(j).getID()){
				Objects.at(i).setType(2);
				j = Final_Path.size();

				cout << Objects.at(i).getID() << ": Object ID" << endl;
				cout << Objects.at(i).getParent() << ": Object parent" << endl;
			}
		}
	}

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		main_xp = main_x;
		main_yp = main_y;
		main_zp = main_z;


		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check and call events
		glfwPollEvents();
		Do_Movement();


		// Clear the colorbuffer
		glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader.Use();   // <-- Don't forget this one!
		// Transformation matrices
		glm::mat4 projection = glm::perspective(camera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

		// Draw the loaded model
		glm::mat4 model;
		glm::mat4 model2;


		if (keys[GLFW_KEY_U]){
			main_Position += glm::vec3(-0.01, 0.0, 0.0);
		}
		if (keys[GLFW_KEY_J]){
			//if (main_x + 0.4 + 0.01 < main2_x)
			main_Position += glm::vec3(0.01, 0.0, 0.0);
		}
		if (keys[GLFW_KEY_I])
			main_Position += glm::vec3(0.0, 0.01, 0.0);
		if (keys[GLFW_KEY_K])
			main_Position += glm::vec3(0.0, -0.01, 0.0);
		if (keys[GLFW_KEY_O])
			main_Position += glm::vec3(0.0, 0.0, 0.01);

		if (keys[GLFW_KEY_L])
			main_Position += glm::vec3(0.0, 0.0, -0.01);

		//main_Position = glm::vec3(main_x, main_y, main_z);

		model = glm::translate(model, glm::vec3(main_Position)); // Translate it down a bit so it's at the center of the scene
		model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down

		//main_Yaw = glm::normalize(main_Yaw);

		camera.SetYaw();

		main_Yaw = ((camera.GetYaw() - 180) * (M_PI)) / 180;
		main_Pitch = ((camera.GetPitch() - 180) * (M_PI)) / 180;

		//cout << camera.GetYaw() << ": angle_Yaw" << endl;
		//cout << camera.GetPitch() << ": angle_Pitch" << endl;
		//cout << main_Yaw << ": cos" << endl;
		//cout << main_Front.x << ": front" << endl;

		//cout << Objects[1].getConnections().size() << ": Neighbours of object 1x 0y" << endl;
		if (keys[GLFW_KEY_P]){
			cout << Final_Path.size() << ": final path size" << endl;
		}


		//model = glm::rotate(model, -main_Yaw, glm::vec3(0, 1, 0));

		//THIS IS THE ARROW BLOCK
		//glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
		//ourModel.Draw(shader);

		model2 = glm::translate(model2, glm::vec3(main2_x, main2_y, main2_z)); // Translate it down a bit so it's at the center of the scene
		model2 = glm::scale(model2, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
		ourModel2.Draw(shader);

		for (int i = 0; i < Objects.size(); i++){

			glm::mat4 temp_model;
			temp_int = Objects[i].getType();

			temp_model = glm::translate(temp_model, glm::vec3(Objects[i].getPosition())); // Translate it down a bit so it's at the center of the scene
			temp_model = glm::scale(temp_model, glm::vec3(0.2f, 0.2f, 0.2f));	// It's a bit too big for our scene, so scale it down

			Objects[i];
			glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(temp_model));

			if (temp_int == 0)
			{
				GridModel1.Draw(shader);
			}
			if (temp_int == 1)
			{
				GridModel2.Draw(shader);
			}
			if (temp_int == 2)
			{
				GridModel3.Draw(shader);
			}
		}

		// Swap the buffers
		glfwSwapBuffers(window);
	}

	glfwTerminate();
	return 0;
}

#pragma region "User input"

// Moves/alters the camera positions based on user input
void Do_Movement()
{
	// Camera controls
	if (keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, deltaTime);

}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos;

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);

}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

#pragma endregion