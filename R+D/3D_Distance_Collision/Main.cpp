// Std. Includes
#include <string>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "CustomMath.h"

// GLM Mathemtics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other Libs
#include <SOIL.h>

#define _USE_MATH_DEFINES // for C++  
#include <math.h> 

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

float main_xp, main_yp, main_zp;

GLfloat main_Yaw = 0.0f;
GLfloat main_Pitch = 0.0f;

float scale_num = 0.2f;

vector <Model>walls;

GLfloat velocity;

glm::vec3 Distance3D(glm::vec3 input1, glm::vec3 input2){

	float X1 = input1.x;
	float X2 = input2.x;

	float Y1 = input1.y;
	float Y2 = input2.y;

	float Z1 = input1.z;
	float Z2 = input2.z;

	float extra;

	float tempX;
	float tempY;
	float tempZ;

	//X + +
	if ((X1 >= 0) && (X2 >= 0))
	{
		if (X1 < X2){
			tempX = X2 - X1;
		}
		else {
			tempX = X1 - X2;
		}
	}
	//X + -
	if ((X1 >= 0) && (X2 < 0))
	{
		tempX = X1 - X2;
	}
	//X - +
	if ((X1 < 0) && (X2 >= 0))
	{
		tempX = X2 - X1;

	}
	//X - -
	if ((X1 < 0) && (X2 < 0))
	{
		if (X1 > X2){
			tempX = X2 - X1;
			tempX = 0 - tempX;
		}
		else {
			tempX = X1 - X2;
			tempX = 0 - tempX;
		}
	}

	//Y + +
	if ((Y1 >= 0) && (Y2 >= 0))
	{
		if (Y1 < Y2){
			tempY = Y2 - Y1;
		}
		else {
			tempY = Y1 - Y2;
		}
	}
	//Y + -
	if ((Y1 >= 0) && (Y2 < 0))
	{
		tempY = Y1 - Y2;
	}
	//Y - +
	if ((Y1 < 0) && (Y2 >= 0))
	{
		tempY = Y2 - Y1;

	}
	//Y - -
	if ((Y1 < 0) && (Y2 < 0))
	{
		if (Y1 > Y2){
			tempY = Y2 - Y1;
			tempY = 0 - tempY;
		}
		else {
			tempY = Y1 - Y2;
			tempY = 0 - tempY;
		}
	}

	//Z + +
	if ((Z1 >= 0) && (Z2 >= 0))
	{
		if (Z1 < Z2){
			tempZ = Z2 - Z1;
		}
		else {
			tempZ = Z1 - Z2;
		}
	}
	//Z + -
	if ((Z1 >= 0) && (Z2 < 0))
	{
		tempZ = Z1 - Z2;
	}
	//Z - +
	if ((Z1 < 0) && (Z2 >= 0))
	{
		tempZ = Z2 - Z1;

	}
	//Z - -
	if ((Z1 < 0) && (Z2 < 0))
	{
		if (Z1 > Z2){
			tempZ = Z2 - Z1;
			tempZ = 0 - tempZ;
		}
		else {
			tempZ = Z1 - Z2;
			tempZ = 0 - tempZ;
		}
	}

	return glm::vec3(tempX, tempY, tempZ);
}

glm::vec3 Distancever(glm::vec3 input1, glm::vec3 input2){

	float X1 = input1.x;
	float X2 = input2.x;

	float Y1 = input1.y;
	float Y2 = input2.y;

	float Z1 = input1.z;
	float Z2 = input2.z;

	float extra;

	float tempX;
	float tempY;
	float tempZ;

	//X + +
	if ((X1 >= 0) && (X2 >= 0))
	{
		if (X1 < X2){
			tempX = X2 - X1;
		}
		else {
			tempX = X1 - X2;
		}
	}
	//X + -
	if ((X1 >= 0) && (X2 < 0))
	{
		tempX = X1 - X2;
	}
	//X - +
	if ((X1 < 0) && (X2 >= 0))
	{
		tempX = X2 - X1;

	}
	//X - -
	if ((X1 < 0) && (X2 < 0))
	{
		if (X1 > X2){
			tempX = X2 - X1;
			tempX = 0 - tempX;
		}
		else {
			tempX = X1 - X2;
			tempX = 0 - tempX;
		}
	}

	//Y + +
	if ((Y1 >= 0) && (Y2 >= 0))
	{
		if (Y1 < Y2){
			tempY = Y2 - Y1;
		}
		else {
			tempY = Y1 - Y2;
		}
	}
	//Y + -
	if ((Y1 >= 0) && (Y2 < 0))
	{
		tempY = Y1 - Y2;
	}
	//Y - +
	if ((Y1 < 0) && (Y2 >= 0))
	{
		tempY = Y2 - Y1;

	}
	//Y - -
	if ((Y1 < 0) && (Y2 < 0))
	{
		if (Y1 > Y2){
			tempY = Y2 - Y1;
			tempY = 0 - tempY;
		}
		else {
			tempY = Y1 - Y2;
			tempY = 0 - tempY;
		}
	}

	//Z + +
	if ((Z1 >= 0) && (Z2 >= 0))
	{
		if (Z1 < Z2){
			tempZ = Z2 - Z1;
		}
		else {
			tempZ = Z1 - Z2;
		}
	}
	//Z + -
	if ((Z1 >= 0) && (Z2 < 0))
	{
		tempZ = Z1 - Z2;
	}
	//Z - +
	if ((Z1 < 0) && (Z2 >= 0))
	{
		tempZ = Z2 - Z1;

	}
	//Z - -
	if ((Z1 < 0) && (Z2 < 0))
	{
		if (Z1 > Z2){
			tempZ = Z2 - Z1;
			tempZ = 0 - tempZ;
		}
		else {
			tempZ = Z1 - Z2;
			tempZ = 0 - tempZ;
		}
	}

	return glm::vec3(tempX, tempY, tempZ);
}

glm::vec2 CV_answer(0,0);
glm::vec2 CV_answer2(0,0);

//
//UPDATE VEC3 LOCATIONS WHEN ROTATING
//

//FIGURE OUT HOW TO DISPLAY CLOESET VEC3 POSITION CORRECTLY

// ADD ANSWER TO SMALLER NUMBER VVVV

float Three_D_midway(float input1, float input2)
{

	float answer;

	//X + +
	if ((input1 >= 0) && (input2 >= 0))
	{
		if (input1 < input2){
			answer = input1 + ((input2 - input1) / 2);
		}
		else {
			answer = input2 + ((input1 - input2) / 2);
		}
	}
	//X + -
	if ((input1 >= 0) && (input2 < 0))
	{
		answer = input2 + ((input1 - input2) / 2);
	}
	//X - +
	if ((input1 < 0) && (input2 >= 0))
	{
		answer = input1 + ((input2 - input1) / 2);

	}
	//X - -
	if ((input1 < 0) && (input2 < 0))
	{
		if (input1 < input2){
			answer = input1 + ((input2 - input1) / 2);
			answer = 0 - answer;
		}
		else {
			answer = input2 + ((input1 - input2) / 2);
			answer = 0 - answer;
		}
	}

	return answer;
}

glm::vec3 triangle_center(Mesh input1, int input_num)
{
	glm::vec3 answer;

	float temp1, temp2, temp3;


	//X
	if ((input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x == input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.x) && (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x == input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.x))
	{
		temp1 = input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x;
	}
	else {

		if (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x != input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.x)
		{
			temp1 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x, input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.x), input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.x);
		}
		else
		{
			if (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x != input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.x)
			{
				temp1 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x, input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.x), input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.x);
			}
			else {
				temp1 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.x, input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.x), input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.x);
			}
		}
	}

	//Y
	if ((input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y == input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.y) && (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y == input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.y))
	{
		temp2 = input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y;
	}
	else{

		if (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y != input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.y)
		{
			temp2 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y, input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.y), input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.y);
		}
		else
		{
			if (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y != input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.y)
			{
				temp2 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y, input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.y), input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.y);
			}
			else {
				temp2 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.y, input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.y), input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.y);
			}
		}
	}

	//Z
	if ((input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z == input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.z) && (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z == input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.z))
	{
		temp3 = input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z;
	}
	else {

		if (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z != input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.z)
		{
			temp3 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z, input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.z), input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.z);
		}
		else
		{
			if (input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z != input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.z)
			{
				temp3 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z, input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.z), input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.z);
			}
			else {
				temp3 = Three_D_midway(Three_D_midway(input1.vertices[input1.Faces[input_num].Vertexes[1]].Position.z, input1.vertices[input1.Faces[input_num].Vertexes[2]].Position.z), input1.vertices[input1.Faces[input_num].Vertexes[0]].Position.z);
			}
		}
	}

	answer = glm::vec3(roundf(temp1 * 1000) / 1000, roundf(temp2 * 1000) / 1000, roundf(temp3 * 1000) / 1000);
	answer = answer * scale_num;

	if (keys[GLFW_KEY_G]){
		int hello = 0;
	}
	return answer;
}

int closest_tri_center(Model input1, Model input2, int input_num)
{

	glm::vec3 BD_float = glm::vec3(Distance3D((input1.position + (triangle_center(input1.meshes[0],input_num))), input2.position));

	float best_distance_float = ((((BD_float.x * 10) * (BD_float.x * 10))) + (((BD_float.y * 10) * (BD_float.y * 10)))) + (((BD_float.z * 10) * (BD_float.z * 10)));

	float original_best = best_distance_float;

	int best_face = input_num;

	for (int i = 0; i < input1.meshes[0].Faces[best_face].Neighbours.size(); i++){
		glm::vec3 tri_test = triangle_center(input1.meshes[0], input1.meshes[0].Faces[best_face].Neighbours[i]);
		glm::vec3 test = glm::vec3(Distance3D((input1.position + (tri_test)), input2.position));

		float test_float = ((((test.x * 10) * (test.x * 10))) + (((test.y * 10) * (test.y * 10)))) + (((test.z * 10) * (test.z * 10)));

		if (test_float < best_distance_float){
			best_distance_float = test_float;

			best_face = input1.meshes[0].Faces[best_face].Neighbours[i];
		}
	}

	return best_face;
}

glm::vec2 closest_vec(Model input1, Model input2, int input_int){

	//input_int = vertices
	//next step is to check if current triangle is closest or not then vertices of that triangle

	//Faces actually = Triangles

	int ctc_answer = closest_tri_center(input1,input2,input_int);

	int current_best_face = ctc_answer;

	CV_answer[0] = 0;

	glm::vec3 current_best = Distancever((input1.position + (input1.meshes[0].vertices[input1.meshes[0].Faces[current_best_face].Vertexes[0]].Position * scale_num)), input2.position);
	
	float current_best_f = ((((current_best.x * 10) * (current_best.x * 10))) + (((current_best.y * 10) * (current_best.y * 10)))) + (((current_best.z * 10) * (current_best.z * 10)));

	for (int i = 0; i < input1.meshes[0].Faces[current_best_face].Vertexes.size(); i++){


		glm::vec3 in1_ver_pos = (input1.position + (input1.meshes[0].vertices[i].Position * scale_num));

		glm::vec3 in2_ver_pos = input2.position;


		glm::vec3 next_ver = Distancever(in1_ver_pos, in2_ver_pos);


		glm::vec3 next_vec = Distancever((input1.position + (input1.meshes[0].vertices[input1.meshes[0].Faces[current_best_face].Vertexes[i]].Position * scale_num)), input2.position);
		
		float next_ver_f = ((((next_vec.x * 10) * (next_vec.x * 10))) + (((next_vec.y * 10) * (next_vec.y * 10)))) + (((next_vec.z * 10) * (next_vec.z * 10)));


		if (next_ver_f <= current_best_f){
			current_best = Distancever(in1_ver_pos, in2_ver_pos);
			current_best_f = next_ver_f;
			CV_answer[0] = i;
			CV_answer[1] = current_best_face;
		}


	}

	
	return CV_answer;
}

glm::vec2 closest_vec2(Model input1, Model input2, int input_int){

	//input_int = vertices
	//next step is to check if current triangle is closest or not then vertices of that triangle

	//Faces actually = Triangles



	int ctc_answer = closest_tri_center(input1, input2, input_int);

	int current_best_face = ctc_answer;

	CV_answer2[0] = 0;

	glm::vec3 current_best = Distancever((input1.position + (input1.meshes[0].vertices[input1.meshes[0].Faces[current_best_face].Vertexes[0]].Position * scale_num)), input2.position);

	float current_best_f = ((((current_best.x * 10) * (current_best.x * 10))) + (((current_best.y * 10) * (current_best.y * 10)))) + (((current_best.z * 10) * (current_best.z * 10)));

	for (int i = 0; i < input1.meshes[0].Faces[current_best_face].Vertexes.size(); i++){


		glm::vec3 in1_ver_pos = (input1.position + (input1.meshes[0].vertices[i].Position * scale_num));

		glm::vec3 in2_ver_pos = input2.position;


		glm::vec3 next_ver = Distancever(in1_ver_pos, in2_ver_pos);


		glm::vec3 next_vec = Distancever((input1.position + (input1.meshes[0].vertices[input1.meshes[0].Faces[current_best_face].Vertexes[i]].Position * scale_num)), input2.position);
		//VVVV
		float next_ver_f = ((((next_vec.x * 10) * (next_vec.x * 10))) + (((next_vec.y * 10) * (next_vec.y * 10)))) + (((next_vec.z * 10) * (next_vec.z * 10)));


		if (next_ver_f <= current_best_f){
			current_best = Distancever(in1_ver_pos, in2_ver_pos);
			current_best_f = next_ver_f;
			CV_answer2[0] = i;
			CV_answer2[1] = current_best_face;
		}


	}

	return CV_answer2;
}


bool CheckCollision(Model input1, Model input2){
	if (input1.position[0] + ((input1.width)* scale_num / 2) > input2.position[0] - (((input2.width)* scale_num / 2)) - 0.01){
		if (input1.position[0] - ((input1.width)* scale_num / 2) < input2.position[0] + (((input2.width)* scale_num / 2)) + 0.01){
			if (input1.position[1] + ((input1.height)* scale_num / 2) > input2.position[1] - (((input2.height)* scale_num / 2)) - 0.01){
				if (input1.position[1] - ((input1.height)* scale_num / 2) < input2.position[1] + (((input2.height)* scale_num / 2)) + 0.01){
					if (input1.position[2] + ((input1.depth)* scale_num / 2) > input2.position[2] - (((input2.depth)* scale_num / 2)) - 0.01){
						if (input1.position[2] - ((input1.depth)* scale_num / 2) < input2.position[2] + (((input2.depth)* scale_num / 2)) + 0.01){
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

//MAKE A NEW CHECKCOLLISION WHICH INVOLVES FACE NORMALS

bool CheckCollision2(Model input1, Model input2){
	for (int i = 0; i < input2.meshes.at(0).vertices.size(); i++){
		if (((input1.position.z) + ((input1.width / 2))*scale_num) > ((input2.lowest_z*scale_num) - ((input1.width / 2))*scale_num) - velocity){
			if (((input1.position.z) + ((input1.width / 2))*scale_num) < ((input2.highest_z*scale_num) + ((input1.width / 2))*scale_num) + velocity){
				if (((input1.position.y) + ((input1.width / 2))*scale_num) > ((input2.lowest_y*scale_num) - ((input1.width / 2))*scale_num) - velocity){
					if (((input1.position.y) + ((input1.width / 2))*scale_num) < ((input2.highest_y*scale_num) + ((input1.width / 2))*scale_num) + velocity){
						if (input2.lowest_x == input2.highest_x){
							if (((input1.position.x) + ((input1.width / 2)*scale_num)) > ((input2.position.x) + (((input2.meshes.at(0).vertices.at(i).Normal.x * 3.0f) * scale_num) / 10))){

								//cout << "x1 " << i << endl;

								if ((input1.position.x + ((input1.width / 2)*scale_num)) < ((input2.position.x) + (((input2.meshes.at(0).vertices.at(i).Normal.x) * scale_num) / 10))){
									//cout << "x2 " << i << endl;

									cout << "normal * 3.0f " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x * 3.0f)* scale_num) / 10 << endl;
									cout << "normal " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x)* scale_num) / 10 << endl;

									int stop_here;
									stop_here = 0;

									return true;

								}
							}
							if (((input1.position.x) - ((input1.width / 2)*scale_num)) > ((input2.position.x) + (((input2.meshes.at(0).vertices.at(i).Normal.x * 3.0f) * -scale_num) / 10))){
								if ((input1.position.x - ((input1.width / 2)*scale_num)) < ((input2.position.x) + (((input2.meshes.at(0).vertices.at(i).Normal.x) * -scale_num) / 10))){
									//cout << "x2 " << i << endl;

									cout << "normal * 3.0f " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x * 3.0f)* scale_num) / 10 << endl;
									cout << "normal " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x)* scale_num) / 10 << endl;

									int stop_here;
									stop_here = 0;

									return true;

								}
							}
						}
					}
				}
			}
		}
#pragma region collision_Y
		if (input2.lowest_y == input2.highest_y){
			if (((input1.position.y) + ((input1.width / 2)*scale_num)) > ((input2.position.y) + (((input2.meshes.at(0).vertices.at(i).Normal.y * 3.0f) * scale_num) / 10)) - 0.5){
				//cout << "y1 " << i << endl;
				if ((input1.position.y + ((input1.width / 2)*scale_num)) < ((input2.position.y) + (((input2.meshes.at(0).vertices.at(i).Normal.y) * scale_num) / 10)) - 0.5){
					//cout << "y2 " << i << endl;
					int stop_here;
					stop_here = 0;

					return true;
				}
			}
		}
#pragma endregion

		//WORKING HERE
		//WORKING HERE
		//WORKING HERE
		//WORKING HERE
		//WORKING HERE
		//WORKING HERE


		if (((input1.position.x) + (((input1.width / 2))*scale_num) > (input2.lowest_x*scale_num) - ((input1.width / 2))*scale_num) - velocity){
			if (((input1.position.x) + (((input1.width / 2))*scale_num) < (input2.highest_x*scale_num) + ((input1.width / 2))*scale_num) + velocity){
				if (((input1.position.y) + (((input1.width / 2))*scale_num) > (input2.lowest_y*scale_num) - ((input1.width / 2))*scale_num) - velocity){
					if (((input1.position.y) + (((input1.width / 2))*scale_num) < (input2.highest_y*scale_num) + ((input1.width / 2))*scale_num) + velocity){
						if (input2.lowest_z == input2.highest_z){
							if (((input1.position.z) + ((input1.width / 2)*scale_num)) > ((input2.position.z) + (((input2.meshes.at(0).vertices.at(i).Normal.z * 3.0f) * scale_num) / 10))){
								//cout << "z1 " << i << endl;
								if ((input1.position.z + ((input1.width / 2)*scale_num)) < ((input2.position.z) + (((input2.meshes.at(0).vertices.at(i).Normal.z) * scale_num) / 10))){
									//cout << "z2 " << i << endl;

									cout << "normal * 3.0f " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x * 3.0f)* scale_num) / 10 << endl;
									cout << "normal " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x)* scale_num) / 10 << endl;

									int stop_here;
									stop_here = 0;

									return true;
								}
							}
							if (((input1.position.z) - ((input1.width / 2)*scale_num)) > ((input2.position.z) + (((input2.meshes.at(0).vertices.at(i).Normal.z * 3.0f) * -scale_num) / 10))){
								//cout << "z1 " << i << endl;
								if ((input1.position.z - ((input1.width / 2)*scale_num)) < ((input2.position.z) + (((input2.meshes.at(0).vertices.at(i).Normal.z) * -scale_num) / 10))){
									//cout << "z2 " << i << endl;

									cout << "normal * 3.0f " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x * 3.0f)* scale_num) / 10 << endl;
									cout << "normal " << (input2.position.x) + ((input2.meshes.at(0).vertices.at(i).Normal.x)* scale_num) / 10 << endl;

									int stop_here;
									stop_here = 0;

									return true;
								}
							}
						}
					}
				}
			}
		}

	}
	return false;
}

bool CheckCollision3(Model input1, Model input2){
	// DEPENDING ON WHERE THE WALL IS FIGURE OUT WHICH WAY TO CHECK FOR COLLISION

	// nonsense sudo-code

	// if wall.x < center.x 
	//	then check if collision.x < wall.x

	// if wall.x > center.x
	//	then check if colliison.x > wall.x

	if (((input1.position.x)*scale_num) > ((input2.position.x + input2.model_Right.x)*scale_num) ){
		/*if (((input1.position.y)*scale_num) > ((input2.position.y + input2.model_Right.y)*scale_num) - velocity){
			if (((input1.position.z)*scale_num) > ((input2.position.z + input2.model_Right.z)*scale_num) - velocity){
				if (((input1.position.x)*scale_num) < ((input2.position.x - input2.model_Right.x)*scale_num) + velocity){
					if (((input1.position.y)*scale_num) < ((input2.position.y - input2.model_Right.y)*scale_num) + velocity){
						if (((input1.position.z)*scale_num) < ((input2.position.z - input2.model_Right.z)*scale_num) + velocity){

							if (((input1.position.x)*scale_num) > ((input2.position.x + input2.model_Up.x)*scale_num) - velocity){
								if (((input1.position.y)*scale_num) > ((input2.position.y + input2.model_Up.y)*scale_num) - velocity){
									if (((input1.position.z)*scale_num) > ((input2.position.z + input2.model_Up.z)*scale_num) - velocity){
										if (((input1.position.x )*scale_num) < ((input2.position.x - input2.model_Up.x)*scale_num) + velocity){
											if (((input1.position.y )*scale_num) < ((input2.position.y - input2.model_Up.y)*scale_num) + velocity){
												if (((input1.position.z )*scale_num) < ((input2.position.z - input2.model_Up.z)*scale_num) + velocity){

													if (((input1.position.x + input1.model_Front.x)*scale_num) > ((input2.position.x + input2.model_Front.x)*scale_num) - velocity){
														if (((input1.position.y + input1.model_Front.y)*scale_num) > ((input2.position.y + input2.model_Front.y)*scale_num) - velocity){
															if (((input1.position.z + input1.model_Front.z)*scale_num) > ((input2.position.z + input2.model_Front.z)*scale_num) - velocity){
																if (((input1.position.x - input1.model_Front.x)*scale_num) < ((input2.position.x - input2.model_Front.x)*scale_num) + velocity){
																	if (((input1.position.y - input1.model_Front.y)*scale_num) < ((input2.position.y - input2.model_Front.y)*scale_num) + velocity){
																		if (((input1.position.z - input1.model_Front.z)*scale_num) < ((input2.position.z - input2.model_Front.z)*scale_num) + velocity){

																			cout << "contact" << endl;

																			return true;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}*/
		cout << "contact" << endl;
		return true;
	}

	return false;
}

bool CheckCollision4(Model input1, Model input2){

	if (((input1.position.x) + velocity) > ((5.0f)*scale_num)){
		cout << "contact v2" << endl;
		return true;
	}

	return false;
}



/*

math_Front.x = cos(main_Yaw) * cos(main_Pitch);
math_Front.y = sin(glm::radians(camera.GetPitch()))0;
math_Front.z = sin(main_Yaw) * cos(main_Pitch);
main_Front = glm::normalize(math_Front);*/

//main_Position += main_Front * distance;

//maybe this
//collision area = normal * distance

// The MAIN function, from here we start our application and run our Game loop
int main()
{
	// Init GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "3D Distance / Collision", nullptr, nullptr); // Windowed
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW to setup the OpenGL Function pointers
	glewExperimental = GL_TRUE;
	glewInit();

	// Define the viewport dimensions
	glViewport(0, 0, screenWidth, screenHeight);

	// Setup some OpenGL options
	glEnable(GL_DEPTH_TEST);

	// Setup and compile our shaders
	Shader shader("nano.vs", "nano.frag");

	// Load models
	//Model ourModel("Mesh_Files/nanosuit.obj");
	Model ourModel("Blender Output/test.obj");
	Model ourModel2("Blender Output/test2.obj");
	Model ourModel3("Blender Output/test3.obj");
	Model ourModel4("Blender Output/test4.obj");


	walls.push_back(ourModel2);



	ourModel2.meshes.at(0).indices.at(0);

	ourModel2.position = glm::vec3(1.0,0.0,0.0);


	// Draw in wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	float main_x = 0.0;
	float main_y = 0.0;
	float main_z = 0.0;

	float main2_x = 0.0;
	float main2_y = -0.5;
	float main2_z = 0.0;

	glm::vec3 main_Position;
	glm::vec3 main_Front = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 main_Up;
	glm::vec3 main_Right;
	glm::vec3 world_Up = glm::vec3(0.0f, 1.0f, 0.0f);


	//GLfloat velocity;
	GLfloat SPEED = 3.0f;

	for (int i = 0; i < walls.size(); i++){
		walls.at(i).position = glm::vec3((walls.at(i).highest_x - (walls.at(i).width / 2)) * scale_num, (walls.at(i).highest_y - (walls.at(i).height / 2)) * scale_num, (walls.at(i).highest_z - (walls.at(i).depth / 2)) * scale_num);
	}

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		main_xp = main_x;
		main_yp = main_y;
		main_zp = main_z;


		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check and call events
		glfwPollEvents();
		Do_Movement();

		//main_Position = glm::vec3(main_x, main_y, main_z);


		camera.UpdateLocation(main_Position);

		glm::vec3 math_Front;

		math_Front.x = cos(main_Yaw) * cos(main_Pitch);
		math_Front.y = /*sin(glm::radians(camera.GetPitch()))*/0;
		math_Front.z = sin(main_Yaw) * cos(main_Pitch);
		main_Front = glm::normalize(math_Front);

		main_Right = glm::normalize(glm::cross(main_Front, world_Up));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		main_Up = glm::normalize(glm::cross(main_Right, main_Front));

		// Clear the colorbuffer
		glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader.Use();   // <-- Don't forget this one!
		// Transformation matrices
		glm::mat4 projection = glm::perspective(camera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

		// Draw the loaded model
		glm::mat4 model;
		glm::mat4 model2;
		glm::mat4 model3;
		glm::mat4 model4;
		glm::mat4 model5;
		glm::mat4 model6;




		if (keys[GLFW_KEY_S]){
			velocity = SPEED * deltaTime;

			main_Position -= main_Front * velocity;
		}
		if (keys[GLFW_KEY_A]){
			velocity = SPEED * deltaTime;

			main_Position -= main_Right * velocity;
		}
		if (keys[GLFW_KEY_D]){
			velocity = SPEED * deltaTime;

			main_Position += main_Right * velocity;
		}

		if (keys[GLFW_KEY_LEFT_CONTROL]){
			velocity = SPEED * deltaTime;

			main_Position -= main_Up * velocity;
		}

		if (keys[GLFW_KEY_SPACE]){
			velocity = SPEED * deltaTime;

			main_Position += main_Up * velocity;
		}

		if (keys[GLFW_KEY_U]){
			main_Position += glm::vec3(-0.01, 0.0, 0.0);
		}
		if (keys[GLFW_KEY_J]){
			//if (main_x + 0.4 + 0.01 < main2_x)
			main_Position += glm::vec3(0.01, 0.0, 0.0);
		}
		if (keys[GLFW_KEY_I])
			main_Position += glm::vec3(0.0, 0.01, 0.0);
		if (keys[GLFW_KEY_K])
			main_Position += glm::vec3(0.0, -0.01, 0.0);
		if (keys[GLFW_KEY_O])
			main_Position += glm::vec3(0.0, 0.0, 0.01);

		if (keys[GLFW_KEY_L])
			main_Position += glm::vec3(0.0, 0.0, -0.01);

		if (keys[GLFW_KEY_N]){
			int hello = 0;
		}

		bool checker = false;

		for (int i = 0; i < walls.size(); i++){

			//i = 2;

			if ((CheckCollision2(ourModel, walls.at(i)) == false) && (checker == false)){
				if (keys[GLFW_KEY_W]){
					velocity = SPEED * deltaTime;

					main_Position += main_Front * velocity;
					checker = true;
				}

			}

			/*if (CheckCollision2(ourModel, walls.at(i)) == true){


				//OLD PROBLEM = POSITIONING OF WALLS IS WEIRD

				//CURRENT PROBLEM = CORNER COLLISION IS WEIRD

				if (keys[GLFW_KEY_W]){
					velocity = SPEED * deltaTime;

					if (((walls.at(i).meshes.at(0).vertices.at(0).Normal.x) < 0) && (main_Front.x > 0)){
						main_Position += (main_Front * velocity) * walls.at(i).meshes.at(0).vertices.at(0).Normal;
						//works
					}
					if (((walls.at(i).meshes.at(0).vertices.at(0).Normal.x) > 0) && (main_Front.x < 0)){
						main_Position += (-(main_Front * velocity) * walls.at(i).meshes.at(0).vertices.at(0).Normal);
						//
					}
					if (((walls.at(i).meshes.at(0).vertices.at(0).Normal.z) < 0) && (main_Front.z > 0)){
						main_Position += (main_Front * velocity) * walls.at(i).meshes.at(0).vertices.at(0).Normal;
						//works
					}
					if (((walls.at(i).meshes.at(0).vertices.at(0).Normal.z) > 0 && (main_Front.z < 0))){
						main_Position += (-(main_Front * velocity) * walls.at(i).meshes.at(0).vertices.at(0).Normal);
						//
					}
				}*/

			if (CheckCollision2(ourModel, walls.at(i)) == true){
				
				if (((5.0f) * scale_num) && (main_Front.x > 0)){
					main_Position.x = (((5.0f) * scale_num) * 1.0f) - 0.1f;
				}

				if ((keys[GLFW_KEY_W]) && (main_Front.x < 0)){
					main_Position.x += (main_Front.x * velocity) * 1.0f;
				}

				if (keys[GLFW_KEY_W]){
					if (main_Front.x > 0){
					
						//if (((main_Position.x + main_Front.x) * scale_num) >= ((5.0f) * scale_num)){
						//main_Position.x = ((5.0f) * scale_num);// -((ourModel.width * scale_num) / 2);
						//main_Position.x = ((5.0f) * scale_num) - 0.1f;
						//main_Position.x += ((-(main_Front.x * scale_num) * velocity));
					//}
					}
					
				}
				cout << "this worked" << endl;

				if (ourModel.position[0] + (((ourModel.width)* scale_num) / 2) > walls.at(i).position[0] + ((ourModel.width)* scale_num) / 2){
					if (keys[GLFW_KEY_U]){
						main_Position[0] = walls.at(i).position[0] + ((((walls.at(i).width)* scale_num)) / 2) + ((ourModel.width * scale_num) / 2);
					}
				}
				if (ourModel.position[0] - (((ourModel.width)* scale_num) / 2) < walls.at(i).position[0] - ((ourModel.width)* scale_num) / 2){
					if (keys[GLFW_KEY_J]){
						main_Position[0] = walls.at(i).position[0] - ((((walls.at(i).width)* scale_num)) / 2) - ((ourModel.width * scale_num) / 2);
					}
				}
				if (ourModel.position[1] + (((ourModel.height)* scale_num) / 2) < walls.at(i).position[1] + (((ourModel.height)* scale_num) / 2)){
					if ((keys[GLFW_KEY_I]) || (keys[GLFW_KEY_SPACE])){
						main_Position[1] = walls.at(i).position[1] - ((((walls.at(i).height)* scale_num)) / 2) - ((ourModel.height * scale_num) / 2);
					}
				}
				// if player bottom < platform + player bottom
				if (ourModel.position[1] - (((ourModel.height)* scale_num) / 2) > walls.at(i).position[1] - (((ourModel.height)* scale_num) / 2)){
					if ((keys[GLFW_KEY_K]) || (keys[GLFW_KEY_LEFT_CONTROL])){
						main_Position[1] = walls.at(i).position[1] + ((((walls.at(i).height)* scale_num)) / 2) + ((ourModel.height * scale_num) / 2);
					}
				}
				if (ourModel.position[2] + (((ourModel.depth)* scale_num) / 2) < (walls.at(i).position[2] + ((ourModel.depth)* scale_num) / 2)){
					if (keys[GLFW_KEY_O]){
						main_Position[2] = walls.at(i).position[2] - ((((walls.at(i).depth)* scale_num)) / 2) - ((ourModel.depth * scale_num) / 2);
					}
				}
				if (ourModel.position[2] - (((ourModel.depth)* scale_num) / 2) > walls.at(i).position[2] - ((ourModel.depth)* scale_num) / 2){
					if (keys[GLFW_KEY_L]){
						main_Position[2] = walls.at(i).position[2] + ((((walls.at(i).depth)* scale_num)) / 2) + ((ourModel.depth * scale_num) / 2);
					}
				}
				if (keys[GLFW_KEY_N]){
					int hello = 0;
				}


			}
			//i = walls.size();
		}

		main_Position[0] = roundf(main_Position[0] * 100) / 100;
		main_Position[1] = roundf(main_Position[1] * 100) / 100;
		main_Position[2] = roundf(main_Position[2] * 100) / 100;

		
		//make the vec3 positions of model update with rotation

		ourModel.position = main_Position;

		//main_Position = glm::vec3(main_x, main_y, main_z);

		model = glm::translate(model, glm::vec3(ourModel.position)); // Translate it down a bit so it's at the center of the scene
		model = glm::scale(model, glm::vec3(scale_num, scale_num, scale_num));	// It's a bit too big for our scene, so scale it down

		//main_Yaw = glm::normalize(main_Yaw);

		camera.SetYaw();

		main_Yaw = ((camera.GetYaw() - 180) * (M_PI)) / 180;
		main_Pitch = ((camera.GetPitch() - 180) * (M_PI)) / 180;



		glm::vec3 temp_float = glm::vec3(Distance3D(ourModel.position, ourModel2.position).x, Distance3D(ourModel.position, ourModel2.position).y, Distance3D(ourModel.position, ourModel2.position).z);

		float distance_float = ((temp_float.x * temp_float.x) + (temp_float.y * temp_float.y)) + (temp_float.z * temp_float.z);


		// these are 2 different calculators, 2nd one mostly works????

		closest_vec(ourModel, ourModel2, CV_answer.y);

		closest_vec2(ourModel2, ourModel, CV_answer2.y);

		int close_vec = CV_answer.x;

		int close_vec2 = CV_answer2.x;

		

		//WORKING HERE
		//
		// 1. make current distance solver more accurate -DONE
		// 2. start on collision decision making

		ourModel3.position = ((ourModel.position + ((ourModel.meshes[0].vertices[ourModel.meshes[0].Faces[CV_answer.y].Vertexes[close_vec]].Position) * scale_num)));

		//ourModel3.position = ourModel.position + triangle_center(ourModel.meshes[0],1);
		ourModel4.position = ((ourModel2.position + ((ourModel2.meshes[0].vertices[ourModel2.meshes[0].Faces[CV_answer2.y].Vertexes[close_vec2]].Position) * scale_num)));


		glm::vec3 temp_float2 = Distance3D(ourModel3.position, ourModel2.position);

		float distance_float2 = ((temp_float2.x * temp_float2.x) + (temp_float2.y * temp_float2.y)) + (temp_float2.z * temp_float2.z);


		//cout << camera.GetYaw() << ": angle_Yaw" << endl;
		//cout << camera.GetPitch() << ": angle_Pitch" << endl;
		//cout << cos(main_Yaw) << ": cos" << endl;
		//cout << sin(main_Yaw + 90) << ": sin" << endl;
		//cout << main_Front.x << ": front x" << endl;
		//cout << main_Front.z << ": front z" << endl;
		cout << distance_float << ": distance" << endl;
		cout << CV_answer.y << ": closest face" << endl;
		cout << ourModel.meshes[0].Faces[CV_answer.y].Vertexes[close_vec] << ": closest vec num" << endl;
		cout << distance_float2 << ": closest vec dis" << endl;

		for (int i = 0; i < ourModel.meshes[0].vertices.size(); i++){
			if (i == 6 || i == 9 || i == 10 || i == 23 || i == 3 || i == 11 || i == 21 || i == 15){
				glm::vec3 temp_float3 = Distance3D(ourModel.position + ((ourModel.meshes[0].vertices[i].Position) * scale_num), ourModel2.position);

				float distance_float3 = ((temp_float3.x * temp_float3.x) + (temp_float3.y * temp_float3.y)) + (temp_float3.z * temp_float3.z);

				cout << distance_float3 << ": corner vec dis : " << i << endl;
			}
		}

		if (keys[GLFW_KEY_G]){
			glm::vec3 tri_test2 = triangle_center(ourModel.meshes[0], 1);
			glm::vec3 temp_float4 = Distance3D(ourModel.position + (tri_test2), ourModel2.position);

			float distance_float4 = (((temp_float4.x * 10) * (temp_float4.x * 10)) + ((temp_float4.y * 10) * (temp_float4.y * 10))) + ((temp_float4.z * 10) * (temp_float4.z * 10));

			glm::vec3 tri_test3 = triangle_center(ourModel.meshes[0], 11);
			glm::vec3 temp_float5 = Distance3D(ourModel.position + (tri_test3), ourModel2.position);

			float distance_float5 = (((temp_float5.x * 10) * (temp_float5.x * 10)) + ((temp_float5.y * 10) * (temp_float5.y * 10))) + ((temp_float5.z * 10) * (temp_float5.z * 10));


			glm::vec3 temp_float6 = Distance3D(ourModel.position + ((ourModel.meshes[0].vertices[3].Position) * scale_num), ourModel2.position);

			float distance_float6 = (((temp_float6.x * 10) * (temp_float6.x * 10)) + ((temp_float6.y * 10) * (temp_float6.y * 10))) + ((temp_float6.z * 10) * (temp_float6.z * 10));


			glm::vec3 temp_float7 = Distance3D(ourModel.position + ((ourModel.meshes[0].vertices[23].Position) * scale_num), ourModel2.position);

			float distance_float7 = (((temp_float7.x * 10) * (temp_float7.x * 10)) + ((temp_float7.y * 10) * (temp_float7.y * 10))) + ((temp_float7.z * 10) * (temp_float7.z * 10));
			int hello = 0;
		}
		
		
		model = glm::rotate(model, -main_Yaw, glm::vec3(0, 1, 0));
		//model = glm::rotate(model, -main_Pitch, glm::vec3(0, 0, 1));

		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
		ourModel.Draw(shader);

		model2 = glm::translate(model2, glm::vec3(ourModel2.position)); // Translate it down a bit so it's at the center of the scene
		model2 = glm::scale(model2, glm::vec3(scale_num, scale_num, scale_num));	// It's a bit too big for our scene, so scale it down
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
		ourModel2.Draw(shader);

		
		model3 = glm::translate(model3, glm::vec3(ourModel3.position)); // Translate it down a bit so it's at the center of the scene
		model3 = glm::scale(model3, glm::vec3(scale_num / 2, scale_num / 2, scale_num / 2));	// It's a bit too big for our scene, so scale it down
		model3 = glm::rotate(model3, 0.0f, glm::vec3(0, 1, 0));

		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model3));
		ourModel3.Draw(shader);

		
		

		model4 = glm::translate(model4, glm::vec3(ourModel4.position)); // Translate it down a bit so it's at the center of the scene
		model4 = glm::scale(model4, glm::vec3(scale_num / 2, scale_num / 2, scale_num / 2));	// It's a bit too big for our scene, so scale it down
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model4));
		ourModel4.Draw(shader);

		cout << ourModel3.position.x << " x " << ourModel3.position.y << " y " << ourModel3.position.z << " z" << endl;
		cout << ourModel4.position.x << " x " << ourModel4.position.y << " y " << ourModel4.position.z << " z" << endl;

		/*
		model5 = glm::translate(model5, glm::vec3(ourModel5.position)); // Translate it down a bit so it's at the center of the scene
		model5 = glm::scale(model5, glm::vec3(scale_num, scale_num, scale_num));	// It's a bit too big for our scene, so scale it down
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model5));
		ourModel5.Draw(shader);

		model6 = glm::translate(model6, glm::vec3(ourModel6.position)); // Translate it down a bit so it's at the center of the scene
		model6 = glm::scale(model6, glm::vec3(scale_num, scale_num, scale_num));	// It's a bit too big for our scene, so scale it down
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model6));
		ourModel6.Draw(shader);*/

		// Swap the buffers
		glfwSwapBuffers(window);
		system("cls");
	}

	glfwTerminate();
	return 0;
}

#pragma region "User input"

// Moves/alters the camera positions based on user input
void Do_Movement()
{
	// Camera controls
	if (keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, deltaTime);

}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos;

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);

}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

#pragma endregion