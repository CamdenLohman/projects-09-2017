#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <SOIL.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"

#define _USE_MATH_DEFINES // for C++  
#include <math.h> 

GLint TextureFromFile(const char* path, string directory);

class Model
{
public:

	float lowest_x;
	float highest_x;
	float lowest_y;
	float highest_y;
	float lowest_z;
	float highest_z;

	float width;
	float height;
	float depth;

	glm::vec3 position;
	glm::vec3 model_Front;
	glm::vec3 model_Up;
	glm::vec3 model_Right;

	glm::vec3 math_Front;
	glm::vec3 world_Up = glm::vec3(0.0f, 1.0f, 0.0f);

	

	GLfloat model_Yaw = 0.0f;
	GLfloat model_Pitch = 0.0f;

	/*  Functions   */
	// Constructor, expects a filepath to a 3D model.
	Model(GLchar* path)
	{
		this->loadModel(path);

		model_Yaw = 0;
		model_Pitch = 0;

		math_Front.x = cos(model_Yaw) * cos(model_Pitch);
		math_Front.y = /*sin(glm::radians(model_Pitch))*/0;
		math_Front.z = sin(model_Yaw) * cos(model_Pitch);
		model_Front = glm::normalize(math_Front);

		//model_Front = glm::vec3(meshes.at(0).vertices.at(0).Normal.r, meshes.at(0).vertices.at(0).Normal.s, meshes.at(0).vertices.at(0).Normal.t);
		model_Front = glm::vec3(meshes.at(0).vertices.at(0).Normal.x, meshes.at(0).vertices.at(0).Normal.y, meshes.at(0).vertices.at(0).Normal.z);


		//model_Right = glm::normalize(glm::cross(model_Front, world_Up));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		model_Right = glm::normalize(glm::cross(model_Front, world_Up));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		model_Up = glm::normalize(glm::cross(model_Right, model_Front));
	}

	// Draws the model, and thus all its meshes
	void Draw(Shader shader)
	{
		for (GLuint i = 0; i < this->meshes.size(); i++)
			this->meshes[i].Draw(shader);
	}
	/*  Model Data  */
	vector<Mesh> meshes;
	string directory;
	vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.

private:
	/*  Model Data  */
	//vector<Mesh> meshes;
	//string directory;
	//vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.

	/*  Functions   */
	// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
	void loadModel(string path)
	{
		// Read file via ASSIMP
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
		// Check for errors
		if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
		{
			cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
			return;
		}
		// Retrieve the directory path of the filepath
		this->directory = path.substr(0, path.find_last_of('/'));

		// Process ASSIMP's root node recursively
		this->processNode(scene->mRootNode, scene);
	}

	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene)
	{
		// Process each mesh located at the current node
		for (GLuint i = 0; i < node->mNumMeshes; i++)
		{
			// The node object only contains indices to index the actual objects in the scene. 
			// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
			this->meshes.push_back(this->processMesh(mesh, scene));
		}
		// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
		for (GLuint i = 0; i < node->mNumChildren; i++)
		{
			this->processNode(node->mChildren[i], scene);
		}

	}

	Mesh processMesh(aiMesh* mesh, const aiScene* scene)
	{
		// Data to fill
		vector<Vertex> vertices;
		vector<GLuint> indices;
		vector<Texture> textures;
		vector<Face> faces;

		// Walk through each of the mesh's vertices
		for (GLuint i = 0; i < mesh->mNumVertices; i++)
		{
			Vertex vertex;
			glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
			// Positions
			vector.x = roundf(mesh->mVertices[i].x * 1000) / 1000;
			vector.y = roundf(mesh->mVertices[i].y * 1000) / 1000;
			vector.z = roundf(mesh->mVertices[i].z * 1000) / 1000;
			vertex.Position = vector;

			if (vector.x < lowest_x){
				lowest_x = vector.x;
			}
			if (vector.x > highest_x){
				highest_x = vector.x;
			}

			if (vector.y < lowest_y){
				lowest_y = vector.y;
			}
			if (vector.y > highest_y){
				highest_y = vector.y;
			}

			if (vector.z < lowest_z){
				lowest_z = vector.z;
			}
			if (vector.z > highest_z){
				highest_z = vector.z;
			}

			if ((lowest_x == 0) && (i <= 1)){
				lowest_x = vector.x;
			}
			if ((lowest_y == 0) && (i <= 1)){
				lowest_y = vector.y;
			}
			if ((lowest_z == 0) && (i <= 1)){
				lowest_z = vector.z;
			}

			if ((highest_x == 0) && (i <= 1)){
				highest_x = vector.x;
			}
			if ((highest_y == 0) && (i <= 1)){
				highest_y = vector.y;
			}
			if ((highest_z == 0) && (i <= 1)){
				highest_z = vector.z;
			}

			// Normals
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;

			

			// Texture Coordinates
			if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
			{
				glm::vec2 vec;
				// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
				// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				vertex.TexCoords = vec;
			}
			else
				vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			vertices.push_back(vertex);

		}

		if ((lowest_x > 0) && (lowest_x == highest_x)){
			//lowest_x = 0;
			//highest_x = 0;
		}
		if ((lowest_y > 0) && (lowest_y == highest_y)){
			//lowest_y = 0;
			//highest_y = 0;
		}
		if ((lowest_z > 0) && (lowest_z == highest_z)){
			//lowest_z = 0;
			//highest_z = 0;
		}

		/*if ((lowest_x < 0) && (lowest_x != highest_x))
			lowest_x = lowest_x - (lowest_x * 2);
		if ((lowest_y < 0) && (lowest_y != highest_y))
			lowest_y = lowest_y - (lowest_y * 2);
		if ((lowest_z < 0) && (lowest_z != highest_z))
			lowest_z = lowest_z - (lowest_z * 2);
*/
		if (lowest_x > highest_x)
			lowest_x = highest_x;
		if (lowest_y > highest_y)
			lowest_y = highest_y;
		if (lowest_z > highest_z)
			lowest_z = highest_z;

		if ((lowest_x < 0) && (highest_x > 0)){
			width = highest_x - lowest_x;
		}
		else{
			width = highest_x - lowest_x;
		}
		if ((lowest_y < 0) && (highest_y > 0)){
			height = highest_y - lowest_y;
		}
		else{
			height = highest_y - lowest_y;
		}
		if ((lowest_z < 0) && (highest_z > 0)){
			depth = highest_z - lowest_z;
		}
		else{
			depth = highest_z - lowest_z;
		}
		// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
		for (GLuint i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			vector<int>temp_vectors;
			// Retrieve all indices of the face and store them in the indices vector
			for (GLuint j = 0; j < face.mNumIndices; j++)
			{
				indices.push_back(face.mIndices[j]);
				temp_vectors.push_back(face.mIndices[j]);
			}

			int int_temp = mesh->mFaces[i].mNumIndices;

			

			Face temp(int_temp, temp_vectors);

			faces.push_back(temp);
		}

		bool checker = false;

		//go through faces vector
		for (int i = 0; i < faces.size(); i++)
		{
			//for size of current face vertexes
			for (int j = 0; j < faces[i].Vertexes.size(); j++)
			{
				//check faces vector for matches
				for (int k = 0; k < faces.size(); k++)
				{
					if (i != k){
						for (int l = 0; l < faces[i].Vertexes.size(); l++)
						{
							if (vertices[faces[i].Vertexes[j]].Position == vertices[faces[k].Vertexes[l]].Position){
								for (int m = 0; m < faces[i].Neighbours.size(); m++){
									if (faces[i].Neighbours[m] == k){
										checker = true;
									}
								}
								if (checker == false)
								{
									faces[i].Neighbours.push_back(k);
								}
								else{
									checker = false;
								}
								if (faces[i].Neighbours.size() == 0){
									faces[i].Neighbours.push_back(k);
								}
								
							}
						}
					}
				}
			}
		}
		// Process materials
		if (mesh->mMaterialIndex >= 0)
		{
			aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
			// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
			// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
			// Same applies to other texture as the following list summarizes:
			// Diffuse: texture_diffuseN
			// Specular: texture_specularN
			// Normal: texture_normalN

			// 1. Diffuse maps
			vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
			textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
			// 2. Specular maps
			vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
			textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		}

		// Return a mesh object created from the extracted mesh data
		return Mesh(vertices, indices, textures, faces);
	}

	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName)
	{
		vector<Texture> textures;
		for (GLuint i = 0; i < mat->GetTextureCount(type); i++)
		{
			aiString str;
			mat->GetTexture(type, i, &str);
			// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
			GLboolean skip = false;
			for (GLuint j = 0; j < textures_loaded.size(); j++)
			{
				if (textures_loaded[j].path == str)
				{
					textures.push_back(textures_loaded[j]);
					skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
					break;
				}
			}
			if (!skip)
			{   // If texture hasn't been loaded already, load it
				Texture texture;
				texture.id = TextureFromFile(str.C_Str(), this->directory);
				texture.type = typeName;
				texture.path = str;
				textures.push_back(texture);
				this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
			}
		}
		return textures;
	}
};




GLint TextureFromFile(const char* path, string directory)
{
	//Generate texture ID and load texture data 
	string filename = string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height;
	unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
	// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);

	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	SOIL_free_image_data(image);
	return textureID;
}