//Header

#ifndef WALL_H
#define WALL_H

#include "LTexture.h"
#include "Dot.h"

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

class Wall
{
public:

	int wall_frame = 0;

	float mPosX, mPosY;

	float mVelX, mVelY;

	bool moving_up, moving_down;
	bool BlockDestroyed1 = true;
	bool BlockDestroyed2 = true;
	bool BlockDestroyed3 = true;
	bool BlockDestroyed4 = true;

	int original_id;

	bool empty = false;

	bool hidden = false;

	float OX, OY;
	//Default Constructor
	Wall();
	//Initializes the variables
	Wall(int x, int y, int w, int h,int pic_id,bool Collision,bool moving_up,int OX, int OY);

	//Destructor
	~Wall();
	
	SDL_Rect getRect();

	SDL_Rect getRect2();

	int getPicID();

	void render(SDL_Rect camera, LTexture texture_input, SDL_Renderer* gRenderer);

	void render2(SDL_Rect camera, LTexture texture_input, SDL_Rect* q_block_mode);

	int Get_X();

	int Get_Y();

	int Get_OX();

	int Get_OY();

	int Set_Rectx(int x);

	int Set_Recty(int y);

	void Wall::move2(float timeStep, SDL_Renderer* gRenderer, SDL_Rect camera, LTexture gWallTexture);

	void SetID(int num);

	int GetID();

private:
	
	int mPosW, mPosH;
	bool CC;
	int pic_id;

	//Dot's collision box
	SDL_Rect mCollider;

};

#endif