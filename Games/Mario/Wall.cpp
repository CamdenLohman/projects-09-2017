#include "Wall.h"

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>

//The window renderer
//SDL_Renderer* gRenderer = NULL;

Wall::Wall()
{
	
}

Wall::Wall(int x, int y, int w, int h,int pid,bool Coll_check,bool moving_up,int OriX,int OriY)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;
	mPosW = w;
	mPosH = h;
	pic_id = pid;
	CC = Coll_check;

	OX = OriX;
	OY = OriY;

	//Set collision box dimension
	mCollider.x = mPosX;
	mCollider.y = mPosY;
	mCollider.w = mPosW;
	mCollider.h = mPosH;

}

Wall::~Wall()
{

}


void Wall::render(SDL_Rect camera, LTexture texture_input, SDL_Renderer* gRenderer)
{
	//Show the wall
	if (mPosX <= camera.x + camera.w)
	{
		if (mPosX >= camera.x - 40)
		{//Show the wall relative to the camera
			texture_input.render(mPosX - camera.x, mPosY - camera.y);
		}
	}
}

void Wall::render2(SDL_Rect camera, LTexture texture_input, SDL_Rect* q_block_mode)
{
	//Show the wall
	if (mPosX <= camera.x + camera.w)
	{
		if (mPosX >= camera.x - 40)
		{//Show the wall relative to the camera
			texture_input.render(mPosX - camera.x, mPosY - camera.y, q_block_mode);
		}
	}
}

void Wall::move2(float timeStep, SDL_Renderer* gRenderer, SDL_Rect camera, LTexture gWallTexture)
{
	// MAKE A FOR LOOP FOR CHECKING THROUGH THE PLATFORM VECTOR

	mPosX += mVelX * timeStep;
	mPosY += mVelY * timeStep;
	Set_Rectx(mPosX);
	Set_Recty(mPosY);
	render(camera, gWallTexture,gRenderer);

}

SDL_Rect Wall::getRect()
{
	if (CC)
		return mCollider;

	
}

SDL_Rect Wall::getRect2()
{
	return mCollider;
}

int Wall::getPicID()
{
	return pic_id;
}

int Wall::Get_X()
{
	return mCollider.x;
}

int Wall::Get_Y()
{
	return mCollider.y;
}

int Wall::Get_OX()
{
	return OX;
}

int Wall::Get_OY()
{
	return OY;
}

int Wall::Set_Rectx(int x)
{
	mCollider.x = x;
	
	return mCollider.x;
}

int Wall::Set_Recty( int y)
{
	
	mCollider.y = y;
	return mCollider.y;
}

void Wall::SetID(int num){
	original_id = num;
}

int Wall::GetID(){
	return original_id;
}