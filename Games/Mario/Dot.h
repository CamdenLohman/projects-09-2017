//Header

#ifndef DOT_H
#define DOT_H

#include "Wall.h"
#include "LTexture.h"

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

//The dot that will move around on the screen

class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 16;
	static const int DOT_HEIGHT = 16;

	//Maximum axis velocity of the dot
	static const int DOT_VEL = 250;

	//Initializes the variables
	Dot(float x, float y);

	bool n = true;
	int c = 1;
	int cu = 1;

	bool on_ground = true;
	bool jumping = false;
	bool jumped = true;
	bool moving = false;

	int jump_height;

	//Takes key presses and adjusts the dot's velocity
	void handleEvent(SDL_Event& e, bool ne, int ce, int cue);

	int mPosXPub = 0;
	int mPosYPub = 0;

	float mPosX = mPosXPub;
	float mPosY = mPosYPub;
	float mVelX, mVelY;

	string Texture;

	void level_transfer(bool no, int co, int cuo);

	//Moves the dot
	void move(float timeStep, SDL_Rect camera);


	//Shows the dot on the screen relative to the camera
	void render(SDL_Rect camera, SDL_Renderer* gRenderer, LTexture gDotTexture, SDL_Rect* currentClip, SDL_Rect gSpriteClips[4]);

	void SetTexture();

	string GetTexture();

	//Position accessors
	int getPosX();
	int getPosY();

	SDL_Rect mCollider;

private:



	//Dot's collision box

};


#endif