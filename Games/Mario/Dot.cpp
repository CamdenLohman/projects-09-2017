#include "Dot.h"
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>

#pragma region Dot
Dot::Dot(float x, float y)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;

	//Set collision box dimension
	mCollider.w = DOT_WIDTH;
	mCollider.h = DOT_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
}

//Flip type
SDL_RendererFlip flipType = SDL_FLIP_NONE;

/*
void Dot::level_transfer(bool nl, int co, int cuo)
{

	//THIS IS NOT TELLING IT IS ASKING , JUST LIKE HOW DOT ASKS FOR LOCATION
	if (new_level == true){
		if ((currentlevel_y >= old_level_y) && (T_Direction == 1))
		{
			currentlevel_y = currentlevel_y - 1;
			old_level_y = currentlevel_y;
		}
		if ((currentlevel_y <= old_level_y) && (T_Direction == 2))
		{
			currentlevel_y = currentlevel_y + 1;
			old_level_y = currentlevel_y;
		}
		if ((currentlevel_x >= old_level_x) && (T_Direction == 3))
		{
			currentlevel_x = currentlevel_x - 1;
			old_level_x = currentlevel_x;
		}
		if ((currentlevel_x <= old_level_x) && (T_Direction == 4))
		{
			currentlevel_x = currentlevel_x + 1;
			old_level_x = currentlevel_x;
		}
	}
	co = 1;
	cuo = 1;

}
*/

void Dot::handleEvent(SDL_Event& e, bool nl, int cl_x, int cl_y)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{

		case SDLK_DOWN: break;
		case SDLK_LEFT: mVelX -= DOT_VEL; flipType = SDL_FLIP_HORIZONTAL; moving = true; break;
		case SDLK_RIGHT: mVelX += DOT_VEL; flipType = SDL_FLIP_NONE; moving = true; break;
		//case SDLK_1: new_level = true; currentlevel_x = 1; currentlevel_y = 1; break;
		//case SDLK_2: new_level = true; currentlevel_x = 1; currentlevel_y = 2; break;
		}
		if (on_ground == true)
		{
			switch (e.key.keysym.sym)
			{
			case SDLK_UP:mVelY = 0; mVelY -= DOT_VEL; jumping = true;  on_ground = false; break;
			}
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP:jumping = false; break;
		case SDLK_DOWN: break;
		case SDLK_LEFT: mVelX += DOT_VEL; moving = false; break;
		case SDLK_RIGHT: mVelX -= DOT_VEL; moving = false; break;
		//case SDLK_1: new_level = true; currentlevel_x = 1; currentlevel_y = 1; break;
		//case SDLK_2: new_level = true; currentlevel_x = 1; currentlevel_y = 2; break;
		}
	}
	//n = new_level;
	//c = currentlevel_x;
	//cu = currentlevel_y;
}

void Dot::move(float timeStep, SDL_Rect camera)
{
	//temp
	const int TILE_WIDTH = 16;
	const int TILE_HEIGHT = 16;
	int LEVEL_WIDTH = TILE_WIDTH * 212;
	int LEVEL_HEIGHT = TILE_HEIGHT * 15;
	int MAX_VEL = 800;

	if (mPosX <= camera.x + 2)
	{
		mPosY -= mVelY * timeStep - 2;
		mPosX = camera.x + 3;

	}

	if (mPosX >= LEVEL_WIDTH - DOT_WIDTH)
	{
		mPosX = LEVEL_WIDTH - DOT_WIDTH - 1;
	}

	if (mPosY >= (TILE_HEIGHT * 20))
	{
		mPosY = TILE_HEIGHT * 12;
	}


	//Move the dot up or down

	if (mVelY <= MAX_VEL)
	{

		mVelY += 200;
	}
	else
	{
		mVelY = MAX_VEL;
	}

}

void Dot::render(SDL_Rect camera, SDL_Renderer* gRenderer, LTexture gDotTexture, SDL_Rect* currentClip, SDL_Rect gSpriteClips[4])
{
	if (moving == true)
	{
		//Show the dot relative to the camera
		gDotTexture.render(mPosX - camera.x, mPosY - camera.y, currentClip, NULL, NULL, flipType);
	}
	else
	{
		gDotTexture.render(mPosX - camera.x, mPosY - camera.y, gSpriteClips, NULL, NULL, flipType);
	}
}

int Dot::getPosX()
{
	return mPosX;
}

int Dot::getPosY()
{
	return mPosY;
}

void Dot::SetTexture(){
	Texture = "Images/Player/Player.png";
}

string Dot::GetTexture(){
	return Texture;
}
#pragma endregion
