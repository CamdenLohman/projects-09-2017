#include "Wall.h"

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>

//The dot that will move around on the screen
class Enemy
{
public:
	//The dimensions of the dot
	int ENEMY_WIDTH = 16;
	int ENEMY_HEIGHT = 16;

	int EnemyFrame = 0;

	int e_type;
	int e_state;

	bool moving_left = true;
	bool on_ground = true;

	LTexture Texture;

	//Initializes the variables
	Enemy(float x, float y, int type, int state);



	//Takes key presses and adjusts the dot's velocity
	void handleEvent(SDL_Event& e, bool ne, int ce, int cue);

	int mPosXPub = 0;
	int mPosYPub = 0;

	float mPosX = mPosXPub;
	float mPosY = mPosYPub;

	void level_transfer(bool no, int co, int cuo);

	//Moves the enemy
	void move(float timeStep, vector<Wall>Walls, vector<Wall>transfer_Up, vector<Wall>transfer_Down, vector<Wall>transfer_Left, vector<Wall>transfer_Right);


	//Shows the dot on the screen relative to the camera
	void render(SDL_Rect camera, SDL_Renderer* gRenderer, SDL_RendererFlip flip, LTexture gEnemyTexture, SDL_Rect* EnemycurrentClip);

	void SetTexture(string text);

	LTexture GetTexture();

	//Position accessors
	int getPosX();
	int getPosY();

	SDL_Rect mCollider;

	//Flip type
	SDL_RendererFlip EnemyFlipType = SDL_FLIP_NONE;

	float mVelX, mVelY;
	//Maximum axis velocity of the dot
	int ENEMY_VEL = 250;
	//Dot's collision box
private:
};
