
//Using SDL, SDL_image, standard IO, and, strings
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include "Wall.h"
#include "LTexture.h"
#include "LTimer.h"
#include "Dot.h"
#include "Enemy.h"

//Screen dimension constants

//The dot that will be moving around on the screen


const int TILE_WIDTH = 16;
const int TILE_HEIGHT = 16;

const int SCREEN_WIDTH = TILE_WIDTH * 16;
const int SCREEN_HEIGHT = TILE_HEIGHT * 15;

int LEVEL_WIDTH = TILE_WIDTH * 212;
int LEVEL_HEIGHT = TILE_HEIGHT * 15;

int currentlevel_x = 1;
int currentlevel_y = 1;
int old_level_x = 1;
int old_level_y = 1;
int T_Direction = 0;
int MAX_VEL = 800;



#pragma region TileMaps

int block_break[2][2] =
{
	{ 34, 35 },
	{ 36, 37 }
};

int tilemap1_1[15][212] =
{
	/*row 01*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 02*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 03*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 04*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 12, 13, 13, 13, 14, 0, 0, 0, 0, 0, 9, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 13, 13, 14, 0, 0, 0, 0, 0, 90, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 12, 13, 13, 13, 14, 0, 0, 0, 0, 9, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 12, 13, 13, 13, 14, 0, 0, 0, 0, 9, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 05*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 10, 10, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 06*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 2, 2, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 2, 3, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 07*/{ 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 08*/{ 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	/*row 09*/{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 27, 27, 27, 0, 0, 0, 0, 0, 0 },
	/*row 10*/{ 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 3, 0, 0, 0, 2, 3, 2, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 2, 3, 0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 4, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 29, 30, 31, 0, 0, 0, 0, 0, 0 },
	/*row 11*/{ 0, 0, 20, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0, 0, 0, 0, 0, 0, 7, 8, 0, 0, 20, 0, 0, 0, 0, 0, 0, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 4, 4, 0, 0, 0, 0, 20, 0, 0, 0, 4, 4, 0, 0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 20, 0, 0, 0, 25, 0, 0, 0, 27, 28, 28, 28, 27, 0, 0, 0, 0, 0 },
	/*row 12*/{ 0, 18, 21, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 8, 0, 0, 0, 0, 0, 0, 7, 8, 0, 18, 21, 19, 0, 0, 0, 0, 0, 7, 8, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 21, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 0, 0, 4, 4, 4, 0, 0, 18, 21, 19, 0, 4, 4, 4, 0, 0, 4, 4, 4, 0, 0, 0, 20, 0, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 0, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 18, 21, 19, 0, 0, 25, 0, 0, 0, 26, 26, 32, 26, 26, 0, 0, 20, 0, 0 },
	/*row 13*/{ 18, 21, 22, 23, 19, 0, 0, 0, 0, 0, 0, 15, 16, 16, 16, 17, 18, 21, 19, 0, 0, 0, 0, 15, 16, 17, 0, 0, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 8, 0, 15, 16, 16, 17, 0, 7, 8, 18, 21, 22, 23, 19, 0, 0, 0, 0, 7, 8, 15, 16, 16, 16, 17, 18, 21, 19, 0, 0, 15, 16, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 16, 16, 17, 0, 0, 0, 18, 21, 22, 23, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 16, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 16, 16, 4, 4, 4, 4, 18, 21, 22, 23, 4, 4, 4, 4, 0, 0, 4, 4, 4, 4, 17, 18, 21, 19, 7, 8, 0, 0, 15, 16, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 8, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 18, 21, 22, 23, 19, 0, 4, 0, 0, 0, 26, 26, 33, 26, 26, 17, 18, 21, 19, 0 },
	/*row 14*/{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	/*row 15*/{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
};


#pragma endregion

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Box collision detector
bool checkCollision(SDL_Rect a, SDL_Rect b, vector<Wall>Walls);
bool checkCollisionBlock(SDL_Rect a, SDL_Rect b);

#pragma region bool
bool new_level = true;
bool change_level = false;
bool BlockMove = false;
bool BlockDestroyed1 = false;
bool BlockDestroyed2 = false;
bool BlockDestroyed3 = false;
bool BlockDestroyed4 = false;
bool Pic_Dead = false;
bool e_dead = false;
#pragma endregion

#pragma region int
int map_w = 12;
int map_h = 12;
int cmap_w = 12;
int cmap_h = 12;

int farthest = 0;
#pragma endregion

Dot dot(TILE_WIDTH * 3, TILE_HEIGHT * 8);

//Current animation frame
int frame = 0;
SDL_Rect* currentClip;

const int WALK = 4;
const int G_WALK = 2;
const int K_WALK = 2;
const int q_blox = 5;
SDL_Rect gSpriteClips[WALK];
SDL_Rect gGoombaWalk[G_WALK];
SDL_Rect gKoopaWalk[K_WALK];
SDL_Rect q_block[q_blox];

//Current animation frame

SDL_Rect* EnemycurrentClip;
SDL_Rect* q_block_mode;

//int current_level[12][12];

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Globally used font
TTF_Font* gFont = NULL;

//Scene textures
LTexture gFPSTextTexture;

//Scene textures
LTexture gDotTexture;
LTexture gWallTexture;
LTexture gEnemyTexture;
LTexture qBlockTexture;

SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

bool moving = false;

vector<Wall> Walls;
vector<Wall>transfer_Up;
vector<Wall>transfer_Down;
vector<Wall>transfer_Left;
vector<Wall>transfer_Right;
vector<Wall>break_block;
vector<Enemy>enemies;
vector<Enemy>items;

void LTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

bool init()
{
	dot.SetTexture();

	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Mario", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}

				//Initialize SDL_ttf
				if (TTF_Init() == -1)
				{
					printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load dot texture
	if (!gDotTexture.loadFromFile("Images/Player/Player.png",gRenderer))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}
	else
	{
		gSpriteClips[0].x = 0;
		gSpriteClips[0].y = 32;
		gSpriteClips[0].w = 16;
		gSpriteClips[0].h = 16;

		gSpriteClips[1].x = 15;
		gSpriteClips[1].y = 32;
		gSpriteClips[1].w = 16;
		gSpriteClips[1].h = 16;

		gSpriteClips[2].x = 31;
		gSpriteClips[2].y = 32;
		gSpriteClips[2].w = 16;
		gSpriteClips[2].h = 16;

		gSpriteClips[3].x = 47;
		gSpriteClips[3].y = 32;
		gSpriteClips[3].w = 16;
		gSpriteClips[3].h = 16;
	}
	//Load dot texture
	if (!gEnemyTexture.loadFromFile("Images/enemies/Enemies.png", gRenderer))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}
	else
	{
		gGoombaWalk[0].x = 0;
		gGoombaWalk[0].y = 16;
		gGoombaWalk[0].w = 16;
		gGoombaWalk[0].h = 16;

		gGoombaWalk[1].x = 15;
		gGoombaWalk[1].y = 16;
		gGoombaWalk[1].w = 16;
		gGoombaWalk[1].h = 16;

		gKoopaWalk[0].x = 96;
		gKoopaWalk[0].y = 8;
		gKoopaWalk[0].w = 16;
		gKoopaWalk[0].h = 24;

		gKoopaWalk[1].x = 112;
		gKoopaWalk[1].y = 8;
		gKoopaWalk[1].w = 16;
		gKoopaWalk[1].h = 24;


	}
	if (!qBlockTexture.loadFromFile("65962.png", gRenderer))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}
	else
	{
		q_block[0].x = 80;
		q_block[0].y = 112;
		q_block[0].w = 16;
		q_block[0].h = 16;

		q_block[1].x = 96;
		q_block[1].y = 112;
		q_block[1].w = 16;
		q_block[1].h = 16;

		q_block[2].x = 112;
		q_block[2].y = 112;
		q_block[2].w = 16;
		q_block[2].h = 16;

		q_block[3].x = 128;
		q_block[3].y = 112;
		q_block[3].w = 16;
		q_block[3].h = 16;
	}
	//Open the font
	gFont = TTF_OpenFont("lazy.ttf", 28);
	if (gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}

	return success;
}

void close()
{
	//Free loaded images
	gDotTexture.free();
	gWallTexture.free();
	gEnemyTexture.free();

	//Destroy window	
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

#pragma region Collisions
bool checkCollision(SDL_Rect a, SDL_Rect b, vector<Wall>Walls)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//for (unsigned int i = 0; i < Walls.size(); i++)
	//{
	//SDL_Rect test = Walls[i].getRect();
	//		Lwall = test.x;
	//		Rwall = test.x + test.w;
	//		Twall = test.y;
	//		Bwall = test.y + test.h;
	//If any of the sides from A are outside of B
	if (leftB >= camera.x)
	{
		if (leftB <= camera.x + camera.w)
		{
			if (bottomA <= topB)
			{
				//on_ground = true;
				return false;
			}

			if (bottomA >= topB)
			{
				if (leftA <= rightB)
				{
					if (rightA >= leftB)
					{
						if (topA <= bottomB)
						{
							return true;
						}
					}
				}
			}

			if (topA >= bottomB)
			{
				return false;
			}




			if (rightA <= leftB)
			{
				return false;
			}


			if (leftA >= rightB)
			{
				return false;
			}

		}
		else
		{
			return false;
		}
		//}
		//If none of the sides from A are outside B

		//dot.on_ground = true;
		dot.jump_height = dot.getPosY() - (dot.DOT_HEIGHT * 6);
		return true;
	}
}

bool checkCollisionBlock(SDL_Rect a, SDL_Rect b)
{
	// b = player
	// a = tiles

	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	//Wall block
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	//Player
	leftB = b.x + 7;
	rightB = leftB + 2;
	topB = b.y;
	bottomB = b.y + 2;

	//for (unsigned int i = 0; i < Walls.size(); i++)
	//{
	//SDL_Rect test = Walls[i].getRect();
	//		Lwall = test.x;
	//		Rwall = test.x + test.w;
	//		Twall = test.y;
	//		Bwall = test.y + test.h;
	//If any of the sides from A are outside of B
	if (leftA >= camera.x)
	{
		if (leftA <= camera.x + camera.w)
		{
			if (bottomA >= topB)
			{
				if (leftA <= rightB)
				{
					if (rightA >= leftB)
					{
						if (topA <= bottomB)
						{
							return true;
						}
					}
				}
			}

			if (topA <= bottomB)
			{
				return false;
			}

			if (topA <= bottomB)
			{
				return false;
			}




			if (rightA >= leftB)
			{
				return false;
			}


			if (leftA <= rightB)
			{
				return false;
			}

		}
		else
		{
			return false;
		}
		//}
		//If none of the sides from A are outside B

		//dot.on_ground = true;
		return true;
	}
}

#pragma endregion


void vector_clear()
{
	Walls.clear();
	transfer_Up.clear();
	transfer_Down.clear();
	transfer_Left.clear();
	transfer_Right.clear();
	break_block.clear();
	enemies.clear();
	items.clear();
}


// main down here was line 1481

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			//The camera area


			SDL_Rect test;
			//Set text color as black
			SDL_Color textColor = { 0, 0, 0, 255 };

			//The frames per second timer
			LTimer fpsTimer;

			//In memory text stream
			std::stringstream timeText;

			//Start counting frames per second
			int countedFrames = 0;
			fpsTimer.start();

			LTimer q_block_clock;
			q_block_clock.start();

			LTimer goomba_clock;
			goomba_clock.start();

			LTimer player_clock;
			player_clock.start();

			//Keeps track of time between steps
			LTimer stepTimer;

			//While application is running
			while (!quit)
			{
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}

					//Handle input for the dot
					dot.handleEvent(e, new_level, currentlevel_x, currentlevel_y);
				}

				//Calculate time step
				float timeStep = stepTimer.getTicks() / 1000.f;

				//Move for time step


				//DONT FORGET TO MAKE THIS WORK AGAIN vvvvvvv

				dot.move(timeStep, camera);

				//THIS "STOPS" CAMERA MOVING LEFT

				if (farthest < dot.getPosX())
				{
					farthest = dot.getPosX();
				}

				//Center the camera over the dot
				camera.x = (farthest + Dot::DOT_WIDTH / 2) - SCREEN_WIDTH / 2;
				camera.y = (dot.getPosY() + Dot::DOT_HEIGHT / 2) - SCREEN_HEIGHT / 2;

				//Keep the camera in bounds
				if (camera.x < 0)
				{
					camera.x = 0;
				}
				if (camera.y < 0)
				{
					camera.y = 0;
				}
				if (camera.x > LEVEL_WIDTH - camera.w)
				{
					camera.x = LEVEL_WIDTH - camera.w;
				}
				if (camera.y > LEVEL_HEIGHT - camera.h)
				{
					camera.y = LEVEL_HEIGHT - camera.h;
				}



				//Calculate and correct fps
				float avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);
				if (avgFPS > 2000000)
				{
					avgFPS = 0;
				}

				//Set text to be rendered
				timeText.str("");
				timeText << "Avg FPS " << avgFPS;

				
				#ifdef _SDL_TTF_H
				//Render text
				if (!gFPSTextTexture.loadFromRenderedText(timeText.str().c_str(), textColor,gFont,gRenderer))
				{
					printf("Unable to render FPS texture!\n");
				}
				#endif
				

				//Restart step timer
				stepTimer.start();

				//Clear screen
				SDL_SetRenderDrawColor(gRenderer, 0x6B, 0x8C, 0xFF, 0xFF);
				SDL_RenderClear(gRenderer);

				//Render wall
				SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
				//SDL_RenderDrawRect(gRenderer, &wall);



				//dot.level_transfer(new_level, currentlevel_x, currentlevel_y);

#pragma region T_Direction

				if (T_Direction == 1)
				{
					dot.mPosY = SCREEN_HEIGHT - (TILE_HEIGHT * 3);
					T_Direction = 0;
				}
				if (T_Direction == 2)
				{
					dot.mPosY = TILE_HEIGHT * 2;
					T_Direction = 0;
				}
				if (T_Direction == 3)
				{
					dot.mPosX = SCREEN_WIDTH - (TILE_WIDTH * 3);
					T_Direction = 0;
				}
				if (T_Direction == 4)
				{
					dot.mPosX = TILE_WIDTH * 2;
					T_Direction = 0;
				}

#pragma endregion

				if (new_level == true)
				{

#pragma region level_loader

					vector_clear();
					for (unsigned int i = 0; i < 15; i++)
					{
						for (unsigned int j = 0; j < 212; j++)
						{

							int tiletype;
							if ((currentlevel_x == 1) && (currentlevel_y == 1))
							{
								tiletype = tilemap1_1[i][j];
							}

							bool CC = false;

							if (tiletype == 0)
							{
								CC = false;
							}
							if (tiletype == 1)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wall1.empty = false;
								wall1.hidden = false;
								Walls.push_back(wall1);
							}
							if (tiletype == 2)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wall1.empty = false;
								wall1.hidden = false;
								Walls.push_back(wall1);
							}
							if (tiletype == 3)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wall1.empty = false;
								wall1.hidden = false;
								Walls.push_back(wall1);
							}
							if (tiletype == 4)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wall1.empty = false;
								wall1.hidden = false;
								Walls.push_back(wall1);
							}
							if (tiletype == 5)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wall1.empty = false;
								wall1.hidden = false;
								Walls.push_back(wall1);
							}
							if (tiletype == 6)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wall1.empty = false;
								wall1.hidden = false;
								Walls.push_back(wall1);
							}
							if (tiletype == 7)
							{
								CC = true;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 8)
							{
								CC = true;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 9)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 10)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 11)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 12)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 13)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 14)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 15)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 16)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 17)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 18)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 19)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 20)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 21)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 22)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 23)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 24)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 25)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 26)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 27)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 28)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 29)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 30)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 31)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 32)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}
							if (tiletype == 33)
							{
								CC = false;
								Wall wallT(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								wallT.empty = false;
								wallT.hidden = false;
								Walls.push_back(wallT);
							}


						}
						// COLLISION FOR TRANSFER BLOCKS DOESN'T WORK BUT REST DO, SORT THIS OUT

					}





#pragma endregion

					for (int i = 0; i < Walls.size(); i++){
						Walls[i].SetID(i);
					}

#pragma region enemies
					if ((currentlevel_x == 1) && (currentlevel_y == 1))
					{

						Enemy G1(TILE_WIDTH * 9, TILE_HEIGHT * 9, 1, 1);
						//G1.SetTexture("Images/enemies/Enemies.png");
						enemies.push_back(G1);
						Enemy G2(TILE_WIDTH * 11, TILE_HEIGHT * 9, 1, 1);
						//G2.SetTexture("Images/enemies/Enemies.png");
						enemies.push_back(G2);
						Enemy K1(TILE_WIDTH * 9, TILE_HEIGHT * 3, 2, 1);
						//K1.SetTexture("Images/enemies/Enemies.png");
						enemies.push_back(K1);
						Enemy K2(TILE_WIDTH * 11, TILE_HEIGHT * 3, 2, 1);
						//K2.SetTexture("Images/enemies/Enemies.png");
						enemies.push_back(K2);


					}


#pragma endregion

					new_level = false;
				};


#pragma region WallsVectorImages
				//------------------------------------------------------------
				for (unsigned int i = 0; i < Walls.size(); i++)
				{

#pragma region Normal_Images

					if (Walls[i].getPicID() == 1)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Ground_Brick_Brown.png", gRenderer);
								Walls[i].render(camera,gWallTexture,gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 2)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Brick_Brown.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 3)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								//?-BLOCK
								q_block_mode = &q_block[Walls[i].wall_frame];
								qBlockTexture.loadFromFile("65962.png", gRenderer);

								Walls[i].render2(camera,qBlockTexture,q_block_mode);
							}
						}
					}
					if (Walls[i].getPicID() == 4)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								//SOLID BLOCK
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Solid_Brick_Brown.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 5)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Pipe/Pipe_1_Up_TL.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 6)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Pipe/Pipe_1_Up_TR.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 7)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Pipe/Pipe_1_Left.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 8)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Pipe/Pipe_1_Right.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 9)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Clouds/Cloud_1_Bottom_Left.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 10)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Clouds/Cloud_1_Bottom_Middle.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 11)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Clouds/Cloud_1_Bottom_Right.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 12)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Clouds/Cloud_1_Top_Left.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 13)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Clouds/Cloud_1_Top_Middle.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 14)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Clouds/Cloud_1_Top_Right.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 15)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Bush_L_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 16)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Bush_M_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 17)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Bush_R_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 18)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Hill_Slope_L_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 19)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Hill_Slope_R_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 20)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Hill_Top_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 21)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Hill_Mid_1_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 22)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Hill_Mid_2_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 23)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Hill_Mid_3_Green.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 24)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Flag_Pole_Top.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 25)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Green/Flag_Pole_Middle.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 26)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Brick_Brown_2.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 27)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Top_1.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 28)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Top_2.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 29)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Window_1.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 30)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Window_2.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 31)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Window_3.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 32)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Door_Top.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}
					if (Walls[i].getPicID() == 33)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w + 40)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("Images/Level Tiles/Brown/Castle/Castle_Door_Bottom.png", gRenderer);
								Walls[i].render(camera, gWallTexture, gRenderer);
							}
						}
					}

#pragma endregion

					if (Walls[i].getPicID() == 2)
					{
						SDL_Rect test;
						test.x = dot.getPosX();
						test.y = dot.getPosY();
						test.w = dot.DOT_WIDTH;
						test.h = dot.DOT_HEIGHT;
						if (checkCollisionBlock(Walls[i].getRect(), test) == true)
						{
							dot.mPosX -= dot.mVelX * timeStep;
							dot.mPosY -= dot.mVelY * timeStep;
							BlockMove = true;
							dot.jumping = false;
						}
						if (BlockMove == true)
						{
							Walls[i].mPosY = 50;
							BlockMove = false;
							dot.jumping = false;
							dot.mPosY = Walls[i].Get_Y() + TILE_HEIGHT + 1;
							dot.mVelY = 0;

#pragma region Block_debry_maker
							// X Y+
							Wall BL(Walls[i].Get_X(), Walls[i].Get_Y() + (TILE_HEIGHT / 2), (TILE_WIDTH / 2), (TILE_HEIGHT / 2), 36, false, false, Walls[i].Get_X(), Walls[i].Get_Y() + (TILE_HEIGHT / 2));
							BL.BlockDestroyed1 = true;
							BL.BlockDestroyed2 = true;
							BL.BlockDestroyed3 = true;
							BL.BlockDestroyed4 = true;
							break_block.push_back(BL);
							// X+ Y+
							Wall BR(Walls[i].Get_X() + (TILE_WIDTH / 2), Walls[i].Get_Y() + (TILE_HEIGHT / 2), (TILE_WIDTH / 2), (TILE_HEIGHT / 2), 37, false, false, Walls[i].Get_X() + (TILE_WIDTH / 2), Walls[i].Get_Y() + (TILE_HEIGHT / 2));
							BR.BlockDestroyed1 = true;
							BR.BlockDestroyed2 = true;
							BR.BlockDestroyed3 = true;
							BR.BlockDestroyed4 = true;
							break_block.push_back(BR);
							// X Y
							Wall TL(Walls[i].Get_X(), Walls[i].Get_Y(), (TILE_WIDTH / 2), (TILE_HEIGHT / 2), 34, false, false, Walls[i].Get_X(), Walls[i].Get_Y());
							TL.BlockDestroyed1 = true;
							TL.BlockDestroyed2 = true;
							TL.BlockDestroyed3 = true;
							TL.BlockDestroyed4 = true;
							break_block.push_back(TL);
							// X+ Y
							Wall TR(Walls[i].Get_X() + (TILE_WIDTH / 2), Walls[i].Get_Y(), (TILE_WIDTH / 2), (TILE_HEIGHT / 2), 35, false, false, Walls[i].Get_X() + (TILE_WIDTH / 2), Walls[i].Get_Y());
							TR.BlockDestroyed1 = true;
							TR.BlockDestroyed2 = true;
							TR.BlockDestroyed3 = true;
							TR.BlockDestroyed4 = true;
							break_block.push_back(TR);
							Walls.erase(Walls.begin() + i);

#pragma endregion

						}
					}
					if (Walls[i].getPicID() == 3)
					{
						SDL_Rect test;
						test.x = dot.getPosX();
						test.y = dot.getPosY();
						test.w = dot.DOT_WIDTH;
						test.h = dot.DOT_HEIGHT;
						if (checkCollisionBlock(Walls[i].getRect(), test) == true)
						{
							dot.mPosX -= dot.mVelX * timeStep;
							dot.mPosY -= dot.mVelY * timeStep;
							BlockMove = true;
							dot.jumping = false;
						}
						if (BlockMove == true)
						{
							BlockMove = false;
							dot.jumping = false;
							dot.mPosY = Walls[i].Get_Y() + TILE_HEIGHT + 1;
							dot.mVelY = 0;
							Walls[i].empty = true;
							cout << Walls[i].GetID() << endl;
						}
					}

				}
#pragma endregion

#pragma region Block_Destruction

				if (BlockDestroyed1 == true)
				{
					for (unsigned int i = 0; i < 2; i++)
					{
						for (unsigned int j = 0; j < 2; j++)
						{
							int tiletype;
							if ((currentlevel_x == 1) && (currentlevel_y == 1))
							{
								tiletype = block_break[i][j];
							}

							bool CC = false;

							if (tiletype == 0)
							{
								CC = false;
							}
							if (tiletype == 34)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								break_block.push_back(wall1);
							}
							if (tiletype == 35)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								break_block.push_back(wall1);
							}
							if (tiletype == 36)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								break_block.push_back(wall1);
							}
							if (tiletype == 37)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j, TILE_HEIGHT * i, TILE_WIDTH, TILE_HEIGHT, tiletype, CC, false, 0, 0);
								break_block.push_back(wall1);
							}
						}
					}
				}
				if (break_block.empty() == true)
				{

				}
				else
				{
					for (unsigned int i = 0; i < break_block.size(); i++){


						if (Pic_Dead == false)
						{
							if (break_block[i].getPicID() == 34)
							{
								if (break_block[i].Get_X() <= camera.x + camera.w + 40)
								{
									if (break_block[i].Get_X() >= camera.x - 40)
									{
										gWallTexture.loadFromFile("Images/Level Tiles/Brown/Brick_Brown_Small.png", gRenderer);

										if (break_block[i].BlockDestroyed1 == true)
										{
											break_block[i].mVelX = -200;
											break_block[i].moving_up = true;
											break_block[i].BlockDestroyed1 = false;
										}
										if (break_block[i].moving_up == true)
										{
											if (break_block[i].mPosY >= (break_block[i].OY - (TILE_HEIGHT * 1)))
											{
												break_block[i].mVelY = -200;
											}
											else
											{

												break_block[i].mVelY = 200;
												break_block[i].moving_up = false;
											}

										}



										break_block[i].move2(timeStep,gRenderer,camera,gWallTexture);
										if (break_block[i].Get_Y() >= ((camera.y + camera.h)))
										{
											break_block.erase(break_block.begin() + i);
											Pic_Dead = true;
										}
									}
								}
							}
						}
						if (Pic_Dead == false)
						{
							if (break_block[i].getPicID() == 35)
							{
								if (break_block[i].Get_X() <= camera.x + camera.w + 40)
								{
									if (break_block[i].Get_X() >= camera.x - 40)
									{


										gWallTexture.loadFromFile("Images/Level Tiles/Brown/Brick_Brown_Small.png", gRenderer);
										if (break_block[i].Get_Y() >= ((camera.y + camera.h)))
										{
											break_block.erase(break_block.begin() + i);
											Pic_Dead = true;
										}
										else
										{
											if (break_block[i].BlockDestroyed2 == true)
											{
												break_block[i].mVelX = 200;
												break_block[i].moving_up = true;
												break_block[i].BlockDestroyed2 = false;
											}
											if (break_block[i].moving_up == true)
											{
												if (break_block[i].mPosY >= (break_block[i].OY - (TILE_HEIGHT * 1)))
												{
													break_block[i].mVelY = -200;
												}
												else
												{

													break_block[i].mVelY = 200;
													break_block[i].moving_up = false;
												}

											}

											break_block[i].move2(timeStep,gRenderer, camera, gWallTexture);
										}
									}
								}
							}
						}
						if (Pic_Dead == false)
						{
							if (break_block[i].getPicID() == 36)
							{
								if (break_block[i].Get_X() <= camera.x + camera.w + 40)
								{
									if (break_block[i].Get_X() >= camera.x - 40)
									{


										gWallTexture.loadFromFile("Images/Level Tiles/Brown/Brick_Brown_Small.png", gRenderer);
										if (break_block[i].Get_Y() >= ((camera.y + camera.h)))
										{
											break_block.erase(break_block.begin() + i);
											Pic_Dead = true;
										}
										if (Pic_Dead == false)
										{
											if (break_block[i].BlockDestroyed3 == true)
											{
												break_block[i].mVelX = -200;
												break_block[i].moving_up = true;
												break_block[i].BlockDestroyed3 = false;
											}
											if (break_block[i].moving_up == true)
											{
												if (break_block[i].mPosY >= (break_block[i].OY - (TILE_HEIGHT * 0.5)))
												{
													break_block[i].mVelY = -200;
												}
												else
												{

													break_block[i].mVelY = 200;
													break_block[i].moving_up = false;
												}

											}
											break_block[i].move2(timeStep, gRenderer, camera, gWallTexture);
										}
									}
								}
							}
						}
						if (Pic_Dead == false)
						{
							if (break_block[i].getPicID() == 37)
							{
								if (break_block[i].Get_X() <= camera.x + camera.w + 40)
								{
									if (break_block[i].Get_X() >= camera.x - 40)
									{


										gWallTexture.loadFromFile("Images/Level Tiles/Brown/Brick_Brown_Small.png", gRenderer);
										if (break_block[i].Get_Y() >= ((camera.y + camera.h)))
										{
											break_block.erase(break_block.begin() + i);
											Pic_Dead = true;
										}
										if (Pic_Dead == false)
										{
											if (break_block[i].BlockDestroyed4 == true)
											{
												break_block[i].mVelX = 200;
												break_block[i].moving_up = true;
												break_block[i].BlockDestroyed4 = false;
											}
											if (break_block[i].moving_up == true)
											{
												if (break_block[i].mPosY >= (break_block[i].OY - (TILE_HEIGHT * 0.5)))
												{
													break_block[i].mVelY = -200;
												}
												else
												{

													break_block[i].mVelY = 200;
													break_block[i].moving_up = false;
												}

											}
											break_block[i].move2(timeStep, gRenderer, camera, gWallTexture);
										}
									}
								}
							}
						}
						Pic_Dead = false;
					}
				}
#pragma endregion

/*
#pragma region TransferVectorImages

				for (unsigned int i = 0; i < transfer_Up.size(); i++){
					if (transfer_Up[i].getPicID() == 127)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("tp_block.bmp");
								transfer_Up[i].render(camera.x, camera.y);
							}
						}
					}

				}

				for (unsigned int i = 0; i < transfer_Down.size(); i++){
					if (transfer_Down[i].getPicID() == 128)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("tp_block.bmp");
								transfer_Down[i].render(camera.x, camera.y);
							}
						}
					}

				}

				for (unsigned int i = 0; i < transfer_Left.size(); i++){
					if (transfer_Left[i].getPicID() == 129)
					{
						if (Walls[i].Get_X() <= camera.x + camera.w)
						{
							if (Walls[i].Get_X() >= camera.x - 40)
							{
								gWallTexture.loadFromFile("tp_block.bmp");
								transfer_Left[i].render(camera.x, camera.y);
							}
						}
					}

				}

				for (unsigned int i = 0; i < transfer_Right.size(); i++){

					if (transfer_Right[i].getPicID() == 130)
					{
						if (Walls[i].Get_X() >= camera.x - 40)
						{
							if (Walls[i].Get_X() <= camera.x + camera.w)
							{
								gWallTexture.loadFromFile("tp_block.bmp");
								transfer_Right[i].render(camera.x, camera.y);
							}
						}
					}
				}
#pragma endregion
*/

#pragma region Enemies

				for (unsigned int i = 0; i < enemies.size(); i++)
				{
					if (enemies[i].e_type == 1)
					{
						enemies[i].move(timeStep, Walls, transfer_Up, transfer_Down, transfer_Left, transfer_Right);
						EnemycurrentClip = &gGoombaWalk[enemies[i].EnemyFrame / 2];

						if (enemies[i].getPosX() <= camera.x + camera.w + 40)
						{
							if (enemies[i].getPosX() >= camera.x - 40)
							{
								gEnemyTexture.loadFromFile("Images/enemies/Enemies.png", gRenderer);

								enemies[i].render(camera, gRenderer, enemies[i].EnemyFlipType, gEnemyTexture, EnemycurrentClip);

							}
						}
					}
					if (enemies[i].e_type == 2)
					{
						enemies[i].move(timeStep, Walls, transfer_Up, transfer_Down, transfer_Left, transfer_Right);
						EnemycurrentClip = &gKoopaWalk[enemies[i].EnemyFrame / 2];

						if (enemies[i].getPosX() <= camera.x + camera.w + 40)
						{
							if (enemies[i].getPosX() >= camera.x - 40)
							{
								gEnemyTexture.loadFromFile("Images/enemies/Enemies.png", gRenderer);

								enemies[i].render(camera, gRenderer, enemies[i].EnemyFlipType, gEnemyTexture, EnemycurrentClip);

							}
						}
					}
				}

#pragma endregion

				//Render dot
				if (player_clock.getTicks() >= 30)
				{
					//

					++frame;

					player_clock.stop();
					player_clock.start();
					if (frame / 2 >= WALK)
					{
						frame = 0;
					}
				}
				currentClip = &gSpriteClips[frame / 2];

				gDotTexture.loadFromFile(dot.GetTexture(), gRenderer);

				dot.render(camera, gRenderer, gDotTexture, currentClip, gSpriteClips);

				//Render textures
				gFPSTextTexture.render((SCREEN_WIDTH - gFPSTextTexture.getWidth()) / 2, (SCREEN_HEIGHT - gFPSTextTexture.getHeight()) / 2);

				//Update screen
				SDL_RenderPresent(gRenderer);
				++countedFrames;
				//Go to next frame
				//++frame;

				//Cycle animation


				for (unsigned int i = 0; i < enemies.size(); i++)
				{
					if (goomba_clock.getTicks() >= 120)
					{
						//
						for (unsigned int i = 0; i < enemies.size(); i++)
						{
							++enemies[i].EnemyFrame;
						}
						goomba_clock.stop();
						goomba_clock.start();
					}

					if (enemies[i].e_type == 1)
					{
						if (enemies[i].EnemyFrame / 2 >= G_WALK)
						{
							enemies[i].EnemyFrame = 0;
						}
					}
					if (enemies[i].e_type == 2)
					{
						if (enemies[i].EnemyFrame / 2 >= K_WALK)
						{
							enemies[i].EnemyFrame = 0;
						}
					}
				}

				for (unsigned int i = 0; i < Walls.size(); i++)
				{
					if (q_block_clock.getTicks() >= 180)
					{
						//
						for (unsigned int i = 0; i < Walls.size(); i++)
						{
							++Walls[i].wall_frame;
						}
						q_block_clock.stop();
						q_block_clock.start();
					}

					//if (q_block_clock.isStarted)
					//{
					if (Walls[i].hidden == false)
					{
						//if not hidden
						if (Walls[i].empty == false)
						{
							//if not empty
							if (Walls[i].wall_frame >= 3)
							{
								Walls[i].wall_frame = 0;
							}
						}
						else
						{
							//if empty
							Walls[i].wall_frame = 3;

						}
					}
					else
					{
						//if hidden
						if ((currentlevel_x = 1) && (currentlevel_y = 1))
						{
#pragma region Q_Blocks
							if (i = 177)
							{
								//Coin
							}
							if (i = 179)
							{
								//Mush
							}
							if (i = 122)
							{
								//Coin
							}
							if (i = 181)
							{
								//Coin
							}
							if (i = 165)
							{
								//1-up
								//Hidden
							}
							if (i = 188)
							{
								//Mush
							}
							if (i = 190)
							{
								//Multi-coin
								//Hidden
							}
							if (i = 135)
							{
								//Coin
							}
							if (i = 192)
							{
								//Star
								//Hidden
							}
							if (i = 193)
							{
								//Coin
							}
							if (i = 194)
							{
								//Coin
							}
							if (i = 136)
							{
								//Mush
							}
							if (i = 195)
							{
								//Coin
							}
							if (i = 141)
							{
								//Coin
							}
							if (i = 142)
							{
								//Coin
							}
							if (i = 205)
							{
								//Coin
							}
#pragma endregion
						}
					}

					//}
				}
				dot.mPosX += dot.mVelX * timeStep;
				dot.mCollider.x = dot.mPosX;

				if (dot.jumping == true)
				{


					if (dot.mPosY >= dot.jump_height)
					{
						dot.mVelY = -200;

					}
					else
					{
						dot.jumping = false;
					}

				}

				dot.mPosY += dot.mVelY * timeStep;
				dot.mVelY = 0;
				dot.mCollider.y = dot.mPosY;

				for (unsigned int i = 0; i < Walls.size(); i++)
				{
					SDL_Rect wall = Walls[i].getRect();
					//If the dot went too far to the left or right
					if (Walls[i].Get_X() <= dot.mCollider.x + TILE_WIDTH)
					{
						if (Walls[i].Get_X() >= dot.mCollider.x - TILE_WIDTH)
						{
							if (checkCollision(dot.mCollider, wall, Walls) == true)
							{
								dot.on_ground = true;
								dot.jump_height = dot.getPosY() - (dot.DOT_HEIGHT * 5);
								//Move back

								//dot.mPosY -= (dot.mVelY * timeStep);
								//dot.mVelY = 0;
								//dot.mCollider.y = dot.mPosY;
								if (dot.mPosY <= Walls[i].Get_Y())
								{
									dot.mPosY = Walls[i].Get_Y() - dot.DOT_HEIGHT;
									dot.mCollider.y = dot.mPosY;
								}
								if (dot.mPosY >= Walls[i].Get_Y() + TILE_HEIGHT)
								{
									dot.mPosY = Walls[i].Get_Y() + TILE_HEIGHT;
									dot.mCollider.y = dot.mPosY;
								}
							}

						}
					}
					for (unsigned int j = 0; j < enemies.size(); j++)
					{
						if (enemies[j].moving_left == true)
						{
							enemies[j].mVelX = -0.05;
							enemies[j].EnemyFlipType = SDL_FLIP_NONE;

						}
						if (enemies[j].moving_left == false)
						{
							enemies[j].mVelX = 0.05;
							enemies[j].EnemyFlipType = SDL_FLIP_HORIZONTAL;
						}

						enemies[j].mPosX += enemies[j].mVelX * timeStep;
						enemies[j].mCollider.x = enemies[j].mPosX;

						enemies[j].mPosY += enemies[j].mVelY * timeStep;
						enemies[j].mVelY = 0;
						enemies[j].mCollider.y = enemies[j].mPosY;

						SDL_Rect test = Walls[i].getRect();
						//If the dot went too far to the left or right
						if (Walls[i].Get_X() <= enemies[j].getPosX() + TILE_WIDTH)
						{
							if (Walls[i].Get_X() >= enemies[j].getPosX() - TILE_WIDTH)
							{
								if (checkCollision(enemies[j].mCollider, test, Walls) == true)
								{
									//Move back

									//dot.mPosY -= (dot.mVelY * timeStep);
									//dot.mVelY = 0;
									//dot.mCollider.y = dot.mPosY;
									if (enemies[j].e_type == 1)
									{
										if (enemies[j].mPosY <= Walls[i].Get_Y())
										{
											enemies[j].mPosY = Walls[i].Get_Y() - enemies[j].ENEMY_HEIGHT;
											enemies[j].mCollider.y = enemies[j].mPosY;
										}
										if (enemies[j].mPosY >= Walls[i].Get_Y() + TILE_HEIGHT)
										{
											enemies[j].mPosY = Walls[i].Get_Y() + TILE_HEIGHT;
											enemies[j].mCollider.y = enemies[j].mPosY;
										}
									}
									if (enemies[j].e_type == 2)
									{
										if (enemies[j].mPosY + 8 <= Walls[i].Get_Y())
										{
											enemies[j].mPosY = Walls[i].Get_Y() - enemies[j].ENEMY_HEIGHT;
											enemies[j].mCollider.y = enemies[j].mPosY;
										}
										if (enemies[j].mPosY >= Walls[i].Get_Y() + TILE_HEIGHT)
										{
											enemies[j].mPosY = Walls[i].Get_Y() + TILE_HEIGHT;
											enemies[j].mCollider.y = enemies[j].mPosY;
										}
									}
								}

							}
						}
						if (enemies[j].mPosY > (camera.y + camera.h + TILE_HEIGHT))
						{
							enemies.erase(enemies.begin() + j);
							e_dead = true;
						}
						if (e_dead == false)
						{
							if (enemies[j].mPosX < (camera.x - (TILE_HEIGHT * 3)))
							{
								enemies.erase(enemies.begin() + j);
							}
						}
						e_dead = false;
					}
				}

				for (unsigned int i = 0; i < Walls.size(); i++)
				{
					SDL_Rect playerRect = Walls[i].getRect();
					//If the dot went too far to the left or right

					//WORKING HERE
					//THIS IS WHERE THE RECTANGLE'S INFORMATION GETS GRABBED OF WHICH CAN SPAWN, WRAP THIS AROUND THE PLAYER

					if (Walls[i].Get_X() <= dot.getPosX() + TILE_WIDTH)
					{
						if (Walls[i].Get_X() >= dot.getPosX() - TILE_WIDTH)
						{
							if (checkCollision(dot.mCollider, playerRect, Walls) == true)
							{
								if (dot.mCollider.y >= Walls[i].Get_Y())
								{
									if (dot.mCollider.y + dot.mCollider.h - 8 <= Walls[i].Get_Y() + 16)
									{
										if (dot.mCollider.x <= Walls[i].Get_X())
										{
											if (dot.mPosX + dot.DOT_WIDTH >= Walls[i].Get_X())
											{

												dot.mPosX -= (dot.mVelX * timeStep);

												dot.mPosX = Walls[i].Get_X() - TILE_WIDTH;
												dot.mCollider.x = dot.mPosX;
											}
											else
											{

											}
										}
										if (dot.mCollider.x >= Walls[i].Get_X())
										{
											if (dot.mPosX >= Walls[i].Get_X())
											{
												dot.mPosX -= (dot.mVelX * timeStep);

												dot.mPosX = Walls[i].Get_X() + TILE_WIDTH;
												dot.mCollider.x = dot.mPosX;
											}
											else
											{

											}
										}
									}
								}
							}
						}
					}
					for (unsigned int j = 0; j < enemies.size(); j++)
					{
						SDL_Rect GoombaRect = Walls[i].getRect();
						//If the dot went too far to the left or right
						if (Walls[i].Get_X() <= enemies[j].getPosX() + TILE_WIDTH)
						{
							if (Walls[i].Get_X() >= enemies[j].getPosX() - TILE_WIDTH)
							{
								if (checkCollision(enemies[j].mCollider, playerRect, Walls) == true)
								{
									if (enemies[j].e_type == 1)
									{
										if (enemies[j].mCollider.y >= Walls[i].Get_Y())
										{
											if (enemies[j].mCollider.y + enemies[j].mCollider.h - 8 <= Walls[i].Get_Y() + 16)
											{
												if (enemies[j].mCollider.x <= Walls[i].Get_X())
												{
													if (enemies[j].mPosX + enemies[j].ENEMY_WIDTH >= Walls[i].Get_X())
													{
														enemies[j].moving_left = !enemies[j].moving_left;
														enemies[j].mPosX -= (enemies[j].mVelX * timeStep);

														enemies[j].mPosX = Walls[i].Get_X() - TILE_WIDTH;
														enemies[j].mCollider.x = enemies[j].mPosX;

													}
													else
													{

													}
												}

												if (enemies[j].mCollider.x >= Walls[i].Get_X())
												{
													if (enemies[j].mPosX >= Walls[i].Get_X())
													{
														enemies[j].moving_left = !enemies[j].moving_left;
														enemies[j].mPosX -= (enemies[j].mVelX * timeStep);

														enemies[j].mPosX = Walls[i].Get_X() + TILE_WIDTH;
														enemies[j].mCollider.x = enemies[j].mPosX;
													}
													else
													{

													}
												}
											}

										}
									}
									if (enemies[j].e_type == 2)
									{
										if (enemies[j].mCollider.y + 8 >= Walls[i].Get_Y())
										{
											if (enemies[j].mCollider.y + enemies[j].mCollider.h - 8 <= Walls[i].Get_Y() + 16)
											{
												if (enemies[j].mCollider.x <= Walls[i].Get_X())
												{
													if (enemies[j].mPosX + enemies[j].ENEMY_WIDTH >= Walls[i].Get_X())
													{
														enemies[j].moving_left = !enemies[j].moving_left;
														enemies[j].mPosX -= (enemies[j].mVelX * timeStep);

														enemies[j].mPosX = Walls[i].Get_X() - TILE_WIDTH;
														enemies[j].mCollider.x = enemies[j].mPosX;

													}
													else
													{

													}
												}

												if (enemies[j].mCollider.x >= Walls[i].Get_X())
												{
													if (enemies[j].mPosX >= Walls[i].Get_X())
													{
														enemies[j].moving_left = !enemies[j].moving_left;
														enemies[j].mPosX -= (enemies[j].mVelX * timeStep);

														enemies[j].mPosX = Walls[i].Get_X() + TILE_WIDTH;
														enemies[j].mCollider.x = enemies[j].mPosX;
													}
													else
													{

													}
												}
											}

										}
									}
								}
							}
						}
					}

				}
				for (unsigned int j = 0; j < enemies.size(); j++)
				{
					for (unsigned int k = 0; k < enemies.size(); k++)
					{
						if (enemies[j].mCollider.x != enemies[k].mCollider.x)
						{
							if (checkCollision(enemies[j].mCollider, enemies[k].mCollider, Walls) == true)
							{
								enemies[j].moving_left = !enemies[j].moving_left;
								//enemies[k].moving_left = !enemies[k].moving_left;
							}
						}
						else
						{

						}
					}

				}


			}
		}








		//Free resources and close SDL
		close();

		return 0;
	}
}


// Was 3243 original / 2293 here when C+P