
#include <iostream>
#include <string>
#include <map>

//tile start
//tile numbers
int Black = 0;
int White = 1;
int Blue = 2;
int Brown = 3;
int Green = 4;
int Red = 5;
int Yellow = 6;
int Tu = 7;
int Td = 8;
int Tl = 9;
int Tr = 10;



int BLACK = (0, 0, 0);
int WHITE = (255, 255, 255);
int BLUE = (0, 0, 255);
int BROWN = (139, 69, 19);
int GREEN = (0, 255, 0);
int RED = (255, 0, 0);
int YELLOW = (255, 255, 0);
int TU = (0, 0, 10);



std::map<std::string, std::string> colours = {
		{ "Black", "BLACK" },
		{ "White", "WHITE" },
		{ "Blue", "BLUE" },
		{ "Brown", "BROWN" },
		{ "Green", "GREEN" },
		{ "Red", "RED" },
		{ "Yellow", "YELLOW" },
		{ "Tu", "TU" }

};

int tilemap1_1[12][12] = { { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };

int tilemap1_2[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };


int tilemap1_3[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 } };

int tilemap2_1[12][12] = { { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };

int tilemap2_2[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };

int tilemap2_3[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 } };

int tilemap3_1[12][12] = { { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };

int tilemap3_2[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 7, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 7 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };

int tilemap3_3[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 } };

int tilemap4_2[12][12] = { { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 5 },
{ 7, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 } };

int tilesize = 40;
int mapw = 12;
int maph = 12;
//tile end


int camx = 400;
int camy = 400;

int worldw = mapw*tilesize;
int worldh = maph*tilesize;


int HALF_WIDTH = int(camx / 2);
int HALF_HEIGHT = int(camy / 2);

int cams = (camx, camy);



int main()
{
	const int cameraX(0);
	const int cameraY(0);

	const int Dis(0);
	const int edge(0);
	const int entities(0);
	const int  grass(0);
	const int  bridge(0);
	const int  water(0);
	const int  new_level(0);
	const int  tup(0);
	const int  c_level(0);
	const int  c_levelx(0);
	const int  c_levely(0);
	const int  old_levelx(0);
	const int  old_levely(0);
	const int  player(0);
	const int  total_level_width(0);
	const int  total_level_height(0);
	const int  level_switch(0);
	const int  row(0);
	const int  column(0);

	//pygame.init()
	//Dis = pygame.display.set_mode(cams, 0, 32)


	//timer = pygame.time.Clock()

	bool keyW = false;
	bool keyS = false;
	bool keyA = false;
	bool keyD = false;

	bool new_level = true;

	int c_levelx = 1;
	int c_levely = 2;
	int c_level = 2;
	int old_levelx = 1;
	int old_levely = 2;
	int level_switch = tilemap1_3;
	int row = 9;
	int column = 9;

	//entities = pygame.sprite.Group()
	//play = pygame.sprite.Group()
	player = new Player(200, 350);
	bridge = []
		grass = []
		water = []
		edge = []
		tup = []


		int x = 0;
	int y = 0;



	total_level_width = len(tilemap1_2[0]) * 40
		total_level_height = len(tilemap1_2) * 40

		cam_aw = total_level_width - 40
		cam_ah = total_level_height - 40

		camera = Camera(complex_camera, cam_aw, cam_ah)
		play.add(player)

		while True:
	timer.tick(60)



		for event in pygame.event.get() :
			if event.type == QUIT :
				pygame.quit()
				sys.exit()
				if event.type == KEYDOWN and event.key == K_w :
					keyW = True
					if event.type == KEYDOWN and event.key == K_s :
						keyS = True
						if event.type == KEYDOWN and event.key == K_a :
							keyA = True
							if event.type == KEYDOWN and event.key == K_d :
								keyD = True
								if event.type == KEYUP and event.key == K_w :
									keyW = False
									if event.type == KEYUP and event.key == K_s :
										keyS = False
										if event.type == KEYUP and event.key == K_a :
											keyA = False
											if event.type == KEYUP and event.key == K_d :
												keyD = False


												Dis.fill(YELLOW)


												player.update(water, edge, tup, keyW, keyS, keyA, keyD, c_levelx, c_levely, new_level)


												new_level = player.new_lev
												c_levelx = player.c_levx
												c_levely = player.c_levy

												if new_level == True:
	//print("level")
	if c_levelx == 1 and c_levely == 1 :
		level_switch = tilemap1_1

		if c_levelx == 1 and c_levely == 2 :
			level_switch = tilemap1_2

			if c_levelx == 1 and c_levely == 3 :
				level_switch = tilemap1_3

				if c_levelx == 2 and c_levely == 1 :
					level_switch = tilemap2_1

					if c_levelx == 2 and c_levely == 2 :
						level_switch = tilemap2_2

						if c_levelx == 2 and c_levely == 3 :
							level_switch = tilemap2_3

							if c_levelx == 3 and c_levely == 1 :
								level_switch = tilemap3_1

								if c_levelx == 3 and c_levely == 2 :
									level_switch = tilemap3_2

									if c_levelx == 3 and c_levely == 3 :
										level_switch = tilemap3_3

										if c_levelx == 4 and c_levely == 2 :
											level_switch = tilemap4_2

											Levels.level_1_2()
											Levels.Transfer()

											for e in entities :
	Dis.blit(e.image, camera.apply(e))
		for e in play :
	Dis.blit(e.image, camera.apply(e))


		camera.update(player)

		//print(new_level)
		new_level = False
		player.c_levx = c_levelx
		player.c_levy = c_levely
		player.new_lev = new_level

		old_levelx = c_levelx
		old_levely = c_levely

		//print(new_level)
		//print(c_level)
		//print(player.rect.x)
		//print(player.rect.y)
		pygame.display.update()

}

class Camera(object)
{
	def __init__(self, camera_func, width, height) :
		self.camera_func = camera_func
		self.state = Rect(0, 0, width, height)

		def apply(self, target) :
		return target.rect.move(self.state.topleft)

		def update(self, target) :
		self.state = self.camera_func(self.state, target.rect)

		def simple_camera(camera, target_rect) :
		l, t, _, _ = target_rect
		_, _, w, h = camera
		return Rect(-l + HALF_WIDTH, -t + HALF_HEIGHT, w, h)

		def complex_camera(camera, target_rect) :
		l, t, _, _ = target_rect
		_, _, w, h = camera
		l, t, _, _ = -l + HALF_WIDTH, -t + HALF_HEIGHT, w, h

		l = min(0 - 40, l)                           # stop scrolling at the left edge
		l = max(-(camera.width - camx), l)   # stop scrolling at the right edge
		t = max(-(camera.height - camy), t) # stop scrolling at the bottom
		t = min(0 - 40, t)                           # stop scrolling at the top
		return Rect(l, t, w, h)
}

class Entity(pygame.sprite.Sprite) {

	def __init__(self) :
		pygame.sprite.Sprite.__init__(self)
}

class Player(Entity) {
	def __init__(self, x, y) :

		Entity.__init__(self)
		self.x_speed = 0
		self.y_speed = 0
		self.c_lev = c_level
		self.new_lev = new_level
		self.c_levx = c_levelx
		self.c_levy = c_levely


		self.image = pygame.Surface((40, 40))
		self.image.fill(Color("#FF0000"))
		//self.image.convert()
		self.rect = Rect(x, y, 40, 40)


		def update(self, water, edge, tup, keyW, keyS, keyA, keyD, c_levelx, c_levely, new_level) :

		if keyW :
			self.y_speed = -2

			if keyS :
				self.y_speed = 2

				if keyA :
					self.x_speed = -2

					if keyD :
						self.x_speed = 2

						self.rect.left += self.x_speed
						self.collide_w(self.x_speed, 0, water)
						self.collide_e(self.x_speed, 0, edge)
						self.collide_t1(self.x_speed, 0, tup, c_levelx, new_level)

						self.rect.top += self.y_speed
						self.collide_w(0, self.y_speed, water)
						self.collide_e(0, self.y_speed, edge)
						self.collide_t1(0, self.y_speed, tup, c_levely, new_level)
						self.x_speed = 0
						self.y_speed = 0

						new_level = self.new_lev
						c_levely = self.c_levy

						//print("update")
						//print(new_level)
						//print(c_level)
						return c_level
						return new_level


						def collide_w(self, x_speed, y_speed, water) :
						for p in water :
	if pygame.sprite.collide_rect(self, p) :
		if x_speed > 0:
	self.rect.right = p.rect.left
		if x_speed < 0 :
			self.rect.left = p.rect.right
			if y_speed > 0 :
				self.rect.bottom = p.rect.top
				if y_speed < 0 :
					self.rect.top = p.rect.bottom

					def collide_e(self, x_speed, y_speed, edge) :
					for e in edge :
	if pygame.sprite.collide_rect(self, e) :
		if x_speed > 0:
	self.rect.right = e.rect.left
		if x_speed < 0 :
			self.rect.left = e.rect.right
			if y_speed > 0 :
				self.rect.bottom = e.rect.top
				if y_speed < 0 :
					self.rect.top = e.rect.bottom

					def collide_t1(self, x_speed, y_speed, tup, c_levelx, new_level) :
					for u in tup :
	if pygame.sprite.collide_rect(self, u) :
		if x_speed > 0:
	self.rect.right = u.rect.left

		//#moving right
		if c_levelx <= old_levelx :
			self.new_lev = True
			self.c_levx = c_levelx + 1
			return self.new_lev
			return self.c_levx
			print("collide")
			print(self.new_lev)
			print(self.c_lev)
			if x_speed < 0:
	self.rect.left = u.rect.right

		//#moving left
		if c_levelx >= old_levelx :
			self.new_lev = True
			self.c_levx = c_levelx - 1
			return self.new_lev
			return self.c_levx
			print("collide")
			print(self.new_lev)
			print(self.c_lev)
			if y_speed > 0:
	self.rect.bottom = u.rect.top

		//#moving down
		if c_levely <= old_levely :
			self.new_lev = True
			self.c_levy = c_levely + 1
			return self.new_lev
			return self.c_levy
			print("collide")
			print(self.new_lev)
			print(self.c_lev)

			if y_speed < 0:
	self.rect.top = u.rect.bottom

		//#moving up
		if c_levely >= old_levely :
			self.new_lev = True
			self.c_levy = c_levely - 1
			print("collide")
			print(self.new_lev)
			print(self.c_lev)
			return self.new_lev
			return self.c_levy
}



class Platform_w(Entity)

{

	def __init__(self, x, y) :
		ct = pygame.Surface((40, 40))

		Entity.__init__(self)

		self.rect = Rect(x, y, tilesize, tilesize)
		self.image = ct
		self.image.fill(Color("#0000FF"))


		def update(self) :
		pass

}

class Platform_b(Entity)

{

	def __init__(self, x, y) :
		ct = pygame.Surface((40, 40))

		Entity.__init__(self)

		self.rect = Rect(x, y, tilesize, tilesize)
		self.image = ct
		self.image.fill(Color("#663300"))


		def update(self) :
		pass
}

class Platform_g(Entity)

{

	def __init__(self, x, y) :
		ct = pygame.Surface((40, 40))

		Entity.__init__(self)

		self.rect = Rect(x, y, tilesize, tilesize)
		self.image = ct
		self.image.fill(Color("#00FF00"))


		def update(self) :
		pass
}

class Platform_e(Entity)

{

	def __init__(self, x, y) :
		ct = pygame.Surface((40, 40))

		Entity.__init__(self)

		self.rect = Rect(x, y, tilesize, tilesize)
		self.image = ct
		self.image.fill(Color("#FF0000"))


		def update(self) :
		pass
}

class Platform_tu(Entity)

{

	def __init__(self, x, y) :
		ct = pygame.Surface((40, 40))


		Entity.__init__(self)

		self.rect = Rect(x, y, tilesize, tilesize)
		self.image = ct
		self.image.fill(Color("#FF0F00"))


		def update(self) :
		pass
}

class Levels()

{
	def __init__(self) :
		self.lev_swit = level_switch


		def level_1_2() :
		entities.empty()
		del water[:]
		del edge[:]
		del tup[:]
		Levels.Loader()
		new_level = False
		return new_level




		def Transfer() :
		if old_levelx < c_levelx :
			player.rect.left = 50
			if old_levelx > c_levelx :
				player.rect.left = total_level_width - 80
				if old_levely < c_levely :
					player.rect.top = 50
					if old_levely > c_levely :
						player.rect.top = total_level_height - 80

						def Loader() :
						entities.empty()
						del water[:]
						del edge[:]
						del tup[:]
						for row in range(maph) :
							for column in range(mapw) :
								if colours[level_switch[row][column]] == GREEN :
									pygame.draw.rect(Dis, level_switch[row][column], (column*tilesize, row*tilesize, tilesize, tilesize))
									g = Platform_g(column*tilesize, row*tilesize)
									grass.append(g)
									entities.add(g)
									if colours[level_switch[row][column]] == BLUE :
										pygame.draw.rect(Dis, level_switch[row][column], (column*tilesize, row*tilesize, tilesize, tilesize))
										p = Platform_w(column*tilesize, row*tilesize)
										water.append(p)
										entities.add(p)
										if colours[level_switch[row][column]] == BROWN :
											pygame.draw.rect(Dis, level_switch[row][column], (column*tilesize, row*tilesize, tilesize, tilesize))
											b = Platform_b(column*tilesize, row*tilesize)
											bridge.append(b)
											entities.add(b)
											if colours[level_switch[row][column]] == RED :
												pygame.draw.rect(Dis, level_switch[row][column], (column*tilesize, row*tilesize, tilesize, tilesize))
												e = Platform_e(column*tilesize, row*tilesize)
												edge.append(e)
												entities.add(e)
												if colours[level_switch[row][column]] == TU :
													pygame.draw.rect(Dis, level_switch[row][column], (column*tilesize, row*tilesize, tilesize, tilesize))
													u = Platform_tu(column*tilesize, row*tilesize)
													tup.append(u)
													entities.add(u)

													new_level = False
													return new_level

}