#include "Wall.h"
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>

//The window renderer
//SDL_Renderer* gRenderer = NULL;

Wall::Wall()
{

}

Wall::Wall(int x, int y, int w, int h, int pid, bool Coll_check)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;
	mPosW = w;
	mPosH = h;
	pic_id = pid;
	CC = Coll_check;

	//Set collision box dimension
	mCollider.x = mPosX;
	mCollider.y = mPosY;
	mCollider.w = mPosW;
	mCollider.h = mPosH;

}

Wall::~Wall()
{

}


/*Wall::render(vector<Wall>& Walls)
{
//Render wall
SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
for (unsigned int i = 0; i < Walls.size(); i++){

SDL_RenderDrawRect(gRenderer, Walls[i]);
}
}
*/
SDL_Rect Wall::getRect()
{
	if (CC)
		return mCollider;


}

SDL_Rect Wall::getRect2()
{
	return mCollider;
}

int Wall::getPicID()
{
	return pic_id;
}

