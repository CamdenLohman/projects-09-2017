//Header

#ifndef WALL_H
#define WALL_H

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

class Wall
{
public:

	//Default Constructor
	Wall();
	//Initializes the variables
	Wall(int x, int y, int w, int h, int pic_id, bool Collision);

	//Destructor
	~Wall();

	SDL_Rect getRect();

	SDL_Rect getRect2();

	int getPicID();

	void render();

private:
	int mPosX, mPosY;
	int mPosW, mPosH;
	bool CC;
	int pic_id;

	//Dot's collision box
	SDL_Rect mCollider;

};

#endif