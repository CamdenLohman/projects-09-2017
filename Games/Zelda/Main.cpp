/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, and, strings
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "Wall.h"

//Screen dimension constants

//The dot that will be moving around on the screen


const int TILE_WIDTH = 40;
const int TILE_HEIGHT = 40;

const int SCREEN_WIDTH = TILE_WIDTH * 12;
const int SCREEN_HEIGHT = TILE_HEIGHT * 12;

int currentlevel_x = 1;
int currentlevel_y = 3;
int old_level_x = 1;
int old_level_y = 2;
int T_Direction = 0;

#pragma region TileMaps

int tilemap1_1[12][12] =
{ { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 8, 8, 5, 5, 5, 5, 5 } };

int tilemap1_2[12][12] = {
		{ 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
		{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
		{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
		{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
		{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
		{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 10 },
		{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 10 },
		{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
		{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
		{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
		{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
		{ 5, 5, 5, 5, 5, 8, 8, 5, 5, 5, 5, 5 } };

int tilemap1_3[12][12] =
{ { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }
};

int tilemap2_1[12][12] =
{ { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 8, 8, 5, 5, 5, 5, 5 }
};

int tilemap2_2[12][12] =
{ { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 8, 8, 5, 5, 5, 5, 5 }
};

int tilemap2_3[12][12] =
{ { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }
};

int tilemap3_1[12][12] =
{ { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 8, 8, 5, 5, 5, 5, 5 }
};

int tilemap3_2[12][12] =
{ { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 9, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 10 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 8, 8, 5, 5, 5, 5, 5 }
};

int tilemap3_3[12][12] =
{ { 5, 5, 5, 5, 5, 7, 7, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }
};

int tilemap4_2[12][12] =
{ { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 5 },
{ 9, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 5 },
{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 }
};
#pragma endregion
//Texture wrapper class
class LTexture
{
public:
	//Initializes variables
	LTexture();

	//Deallocates memory
	~LTexture();

	//Loads image at specified path
	bool loadFromFile(std::string path);

#ifdef _SDL_TTF_H
	//Creates image from font string
	bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
#endif

	//Creates blank texture
	bool createBlank(int width, int height, SDL_TextureAccess = SDL_TEXTUREACCESS_STREAMING);

	//Deallocates texture
	void free();

	//Set color modulation
	void setColor(Uint8 red, Uint8 green, Uint8 blue);

	//Set blending
	void setBlendMode(SDL_BlendMode blending);

	//Set alpha modulation
	void setAlpha(Uint8 alpha);

	//Renders texture at given point
	void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Set self as render target
	void setAsRenderTarget();

	//Gets image dimensions
	int getWidth();
	int getHeight();

	//Pixel manipulators
	bool lockTexture();
	bool unlockTexture();
	void* getPixels();
	void copyPixels(void* pixels);
	int getPitch();
	Uint32 getPixel32(unsigned int x, unsigned int y);

private:
	//The actual hardware texture
	SDL_Texture* mTexture;
	void* mPixels;
	int mPitch;

	//Image dimensions
	int mWidth;
	int mHeight;
};

//The application time based timer
class LTimer
{
public:
	//Initializes variables
	LTimer();

	//The various clock actions
	void start();
	void stop();
	void pause();
	void unpause();

	//Gets the timer's time
	Uint32 getTicks();

	//Checks the status of the timer
	bool isStarted();
	bool isPaused();

private:
	//The clock time when the timer started
	Uint32 mStartTicks;

	//The ticks stored when the timer was paused
	Uint32 mPausedTicks;

	//The timer status
	bool mPaused;
	bool mStarted;
};

//The dot that will move around on the screen
class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 20;
	static const int DOT_HEIGHT = 20;

	//Maximum axis velocity of the dot
	static const int DOT_VEL = 800;

	//Initializes the variables
	Dot(float x, float y);

	bool n = true;
	int c = 1;
	int cu = 1;

	//Takes key presses and adjusts the dot's velocity
	void handleEvent(SDL_Event& e, bool ne, int ce, int cue);

	int mPosXPub = 0;
	int mPosYPub = 0;

	float mPosX = mPosXPub;
	float mPosY = mPosYPub;

	void level_transfer(bool no, int co, int cuo);

	//Moves the dot
	void move(float timeStep, vector<Wall>Walls, vector<Wall>transfer_Up, vector<Wall>transfer_Down, vector<Wall>transfer_Left, vector<Wall>transfer_Right);


	//Shows the dot on the screen
	void render();



private:

	float mVelX, mVelY;

	//Dot's collision box
	SDL_Rect mCollider;
};



//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Box collision detector
bool checkCollision(SDL_Rect a, SDL_Rect b, vector<Wall>Walls);
bool checkCollisionT_Up(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Up);
bool checkCollisionT_Down(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Down);
bool checkCollisionT_Left(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Left);
bool checkCollisionT_Right(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Right);

bool new_level = true;
bool change_level = false;

int map_w = 12;
int map_h = 12;
int cmap_w = 12;
int cmap_h = 12;
//int current_level[12][12];

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Scene textures
LTexture gDotTexture;
LTexture gWallTexture;
#pragma region LTexture

LTexture::LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mPixels = NULL;
	mPitch = 0;
}

LTexture::~LTexture()
{
	//Deallocate
	free();
}

bool LTexture::loadFromFile(std::string path)
{
	//Get rid of preexisting texture
	free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Convert surface to display format
		SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, NULL);
		if (formattedSurface == NULL)
		{
			printf("Unable to convert loaded surface to display format! %s\n", SDL_GetError());
		}
		else
		{
			//Create blank streamable texture
			newTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h);
			if (newTexture == NULL)
			{
				printf("Unable to create blank texture! SDL Error: %s\n", SDL_GetError());
			}
			else
			{
				//Enable blending on texture
				SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);

				//Lock texture for manipulation
				SDL_LockTexture(newTexture, &formattedSurface->clip_rect, &mPixels, &mPitch);

				//Copy loaded/formatted surface pixels
				memcpy(mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h);

				//Get image dimensions
				mWidth = formattedSurface->w;
				mHeight = formattedSurface->h;

				//Get pixel data in editable format
				Uint32* pixels = (Uint32*)mPixels;
				int pixelCount = (mPitch / 4) * mHeight;

				//Map colors				
				Uint32 colorKey = SDL_MapRGB(formattedSurface->format, 0, 0xFF, 0xFF);
				Uint32 transparent = SDL_MapRGBA(formattedSurface->format, 0x00, 0xFF, 0xFF, 0x00);

				//Color key pixels
				for (int i = 0; i < pixelCount; ++i)
				{
					if (pixels[i] == colorKey)
					{
						pixels[i] = transparent;
					}
				}

				//Unlock texture to update
				SDL_UnlockTexture(newTexture);
				mPixels = NULL;
			}

			//Get rid of old formatted surface
			SDL_FreeSurface(formattedSurface);
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

#ifdef _SDL_TTF_H
bool LTexture::loadFromRenderedText(std::string textureText, SDL_Color textColor)
{
	//Get rid of preexisting texture
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
	if (textSurface != NULL)
	{
		//Create texture from surface pixels
		mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
		if (mTexture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}
	else
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}


	//Return success
	return mTexture != NULL;
}
#endif

bool LTexture::createBlank(int width, int height, SDL_TextureAccess access)
{
	//Create uninitialized texture
	mTexture = SDL_CreateTexture(gRenderer, SDL_PIXELFORMAT_RGBA8888, access, width, height);
	if (mTexture == NULL)
	{
		printf("Unable to create blank texture! SDL Error: %s\n", SDL_GetError());
	}
	else
	{
		mWidth = width;
		mHeight = height;
	}

	return mTexture != NULL;
}

void LTexture::free()
{
	//Free texture if it exists
	if (mTexture != NULL)
	{
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
		mPixels = NULL;
		mPitch = 0;
	}
}

void LTexture::setColor(Uint8 red, Uint8 green, Uint8 blue)
{
	//Modulate texture rgb
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void LTexture::setBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(mTexture, blending);
}

void LTexture::setAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void LTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };

	//Set clip rendering dimensions
	if (clip != NULL)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(gRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

void LTexture::setAsRenderTarget()
{
	//Make self render target
	SDL_SetRenderTarget(gRenderer, mTexture);
}

int LTexture::getWidth()
{
	return mWidth;
}

int LTexture::getHeight()
{
	return mHeight;
}

bool LTexture::lockTexture()
{
	bool success = true;

	//Texture is already locked
	if (mPixels != NULL)
	{
		printf("Texture is already locked!\n");
		success = false;
	}
	//Lock texture
	else
	{
		if (SDL_LockTexture(mTexture, NULL, &mPixels, &mPitch) != 0)
		{
			printf("Unable to lock texture! %s\n", SDL_GetError());
			success = false;
		}
	}

	return success;
}

bool LTexture::unlockTexture()
{
	bool success = true;

	//Texture is not locked
	if (mPixels == NULL)
	{
		printf("Texture is not locked!\n");
		success = false;
	}
	//Unlock texture
	else
	{
		SDL_UnlockTexture(mTexture);
		mPixels = NULL;
		mPitch = 0;
	}

	return success;
}

void* LTexture::getPixels()
{
	return mPixels;
}

void LTexture::copyPixels(void* pixels)
{
	//Texture is locked
	if (mPixels != NULL)
	{
		//Copy to locked pixels
		memcpy(mPixels, pixels, mPitch * mHeight);
	}
}

int LTexture::getPitch()
{
	return mPitch;
}

Uint32 LTexture::getPixel32(unsigned int x, unsigned int y)
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32*)mPixels;

	//Get the pixel requested
	return pixels[(y * (mPitch / 4)) + x];
}

#pragma endregion

#pragma region LTimer
LTimer::LTimer()
{
	//Initialize the variables
	mStartTicks = 0;
	mPausedTicks = 0;

	mPaused = false;
	mStarted = false;
}

void LTimer::start()
{
	//Start the timer
	mStarted = true;

	//Unpause the timer
	mPaused = false;

	//Get the current clock time
	mStartTicks = SDL_GetTicks();
	mPausedTicks = 0;
}

void LTimer::stop()
{
	//Stop the timer
	mStarted = false;

	//Unpause the timer
	mPaused = false;

	//Clear tick variables
	mStartTicks = 0;
	mPausedTicks = 0;
}

void LTimer::pause()
{
	//If the timer is running and isn't already paused
	if (mStarted && !mPaused)
	{
		//Pause the timer
		mPaused = true;

		//Calculate the paused ticks
		mPausedTicks = SDL_GetTicks() - mStartTicks;
		mStartTicks = 0;
	}
}

void LTimer::unpause()
{
	//If the timer is running and paused
	if (mStarted && mPaused)
	{
		//Unpause the timer
		mPaused = false;

		//Reset the starting ticks
		mStartTicks = SDL_GetTicks() - mPausedTicks;

		//Reset the paused ticks
		mPausedTicks = 0;
	}
}

Uint32 LTimer::getTicks()
{
	//The actual timer time
	Uint32 time = 0;

	//If the timer is running
	if (mStarted)
	{
		//If the timer is paused
		if (mPaused)
		{
			//Return the number of ticks when the timer was paused
			time = mPausedTicks;
		}
		else
		{
			//Return the current time minus the start time
			time = SDL_GetTicks() - mStartTicks;
		}
	}

	return time;
}

bool LTimer::isStarted()
{
	//Timer is running and paused or unpaused
	return mStarted;
}

bool LTimer::isPaused()
{
	//Timer is running and paused
	return mPaused && mStarted;
}

#pragma endregion


Dot::Dot(float x, float y)
{
	//Initialize the position
	mPosX = x;
	mPosY = y;

	//Set collision box dimension
	mCollider.w = DOT_WIDTH;
	mCollider.h = DOT_HEIGHT;

	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
}

void Dot::level_transfer(bool nl, int co, int cuo)
{

	//THIS IS NOT TELLING IT IS ASKING , JUST LIKE HOW DOT ASKS FOR LOCATION
	if (new_level == true){
		if ((currentlevel_y >= old_level_y) && (T_Direction == 1))
		{
			currentlevel_y = currentlevel_y - 1;
			old_level_y = currentlevel_y;
		}
		if ((currentlevel_y <= old_level_y) && (T_Direction == 2))
		{
			currentlevel_y = currentlevel_y + 1;
			old_level_y = currentlevel_y;
		}
		if ((currentlevel_x >= old_level_x) && (T_Direction == 3))
		{
			currentlevel_x = currentlevel_x - 1;
			old_level_x = currentlevel_x;
		}
		if ((currentlevel_x <= old_level_x) && (T_Direction == 4))
		{
			currentlevel_x = currentlevel_x + 1;
			old_level_x = currentlevel_x;
		}
	}
	co = 1;
	cuo = 1;

}
void Dot::handleEvent(SDL_Event& e, bool nl, int cl_x, int cl_y)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: mVelY -= DOT_VEL; break;
		case SDLK_DOWN: mVelY += DOT_VEL; break;
		case SDLK_LEFT: mVelX -= DOT_VEL; break;
		case SDLK_RIGHT: mVelX += DOT_VEL; break;
		case SDLK_1: new_level = true; currentlevel_x = 1; currentlevel_y = 1; break;
		case SDLK_2: new_level = true; currentlevel_x = 1; currentlevel_y = 2; break;
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP: mVelY += DOT_VEL; break;
		case SDLK_DOWN: mVelY -= DOT_VEL; break;
		case SDLK_LEFT: mVelX += DOT_VEL; break;
		case SDLK_RIGHT: mVelX -= DOT_VEL; break;
		case SDLK_1: new_level = true; currentlevel_x = 1; currentlevel_y = 1; break;
		case SDLK_2: new_level = true; currentlevel_x = 1; currentlevel_y = 2; break;
		}
	}
	n = new_level;
	c = currentlevel_x;
	cu = currentlevel_y;
}

void Dot::move(float timeStep, vector<Wall>Walls, vector<Wall>transfer_Up, vector<Wall>transfer_Down, vector<Wall>transfer_Left, vector<Wall>transfer_Right)
{
	//Move the dot left or right
	mPosX += mVelX * timeStep;
	mCollider.x = mPosX;


	// MAKE A FOR LOOP FOR CHECKING THROUGH THE PLATFORM VECTOR
#pragma region Wall_X
	for (unsigned int i = 0; i < Walls.size(); i++)
	{

		SDL_Rect test = Walls[i].getRect();
		int id = Walls[i].getPicID();
		//If the dot went too far to the left or right
		if (/*(mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH) ||*/ checkCollision(mCollider, test, Walls))
		{
			//Move back
			mPosX -= (mVelX * timeStep);
			mCollider.x = mPosX;

		}
	}
#pragma endregion

#pragma region transfer_Up_X
	for (unsigned int i = 0; i < transfer_Up.size(); i++)
	{

		SDL_Rect test2 = transfer_Up[i].getRect();
		int idT = transfer_Up[i].getPicID();
		//If the dot went too far to the left or right
		if (checkCollisionT_Up(mCollider, test2, transfer_Up))
		{
			//Move back
			mPosX -= (mVelX * timeStep);
			mCollider.x = mPosX;

		}
	}
#pragma endregion

#pragma region transfer_Down_X
	for (unsigned int i = 0; i < transfer_Down.size(); i++)
	{

		SDL_Rect test2 = transfer_Down[i].getRect();
		int idT = transfer_Down[i].getPicID();
		//If the dot went too far to the left or right
		if (checkCollisionT_Down(mCollider, test2, transfer_Down))
		{
			//Move back
			mPosX -= (mVelX * timeStep);
			mCollider.x = mPosX;

		}
		mCollider.x = mPosX;
	}
#pragma endregion
#pragma region transfer_Left_X
	for (unsigned int i = 0; i < transfer_Left.size(); i++)
	{

		SDL_Rect test2 = transfer_Left[i].getRect();
		int idT = transfer_Left[i].getPicID();
		//If the dot went too far to the left or right
		if (checkCollisionT_Left(mCollider, test2, transfer_Left))
		{
			//Move back
			mPosX -= (mVelX * timeStep);
			mCollider.x = mPosX;

		}
		mCollider.x = mPosX;
	}
#pragma endregion
#pragma region transfer_Right_X
	for (unsigned int i = 0; i < transfer_Right.size(); i++)
	{

		SDL_Rect test2 = transfer_Right[i].getRect();
		int idT = transfer_Right[i].getPicID();
		//If the dot went too far to the left or right
		if (checkCollisionT_Right(mCollider, test2, transfer_Right))
		{
			//Move back
			mPosX -= (mVelX * timeStep);
			mCollider.x = mPosX;

		}
		mCollider.x = mPosX;
	}
#pragma endregion
	//Move the dot up or down
	mPosY += mVelY * timeStep;
	mCollider.y = mPosY;

#pragma region Wall_Y
	for (unsigned int i = 0; i < Walls.size(); i++)
	{

		SDL_Rect test = Walls[i].getRect();

		//If the dot went too far up or down
		if (/*(mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT) ||*/ checkCollision(mCollider, test, Walls))
		{
			//Move back
			mPosY -= (mVelY * timeStep);
			mCollider.y = mPosY;
		}
	}
#pragma endregion

#pragma region transfer_Up_Y
	for (unsigned int i = 0; i < transfer_Up.size(); i++)
	{

		SDL_Rect test2 = transfer_Up[i].getRect();

		//If the dot went too far up or down
		if (checkCollisionT_Up(mCollider, test2, transfer_Up))
		{
			//Move back
			mPosY -= (mVelY * timeStep);
			mCollider.y = mPosY;

		}
	}
#pragma endregion

#pragma region transfer_Down_Y
	for (unsigned int i = 0; i < transfer_Down.size(); i++)
	{

		SDL_Rect test2 = transfer_Down[i].getRect();

		//If the dot went too far up or down
		if (checkCollisionT_Down(mCollider, test2, transfer_Down))
		{
			//Move back
			mPosY -= (mVelY * timeStep);
			mCollider.y = mPosY;

		}
		mCollider.y = mPosY;
	}
#pragma endregion

#pragma region transfer_Left_Y
	for (unsigned int i = 0; i < transfer_Left.size(); i++)
	{

		SDL_Rect test2 = transfer_Left[i].getRect();

		//If the dot went too far up or down
		if (checkCollisionT_Left(mCollider, test2, transfer_Left))
		{
			//Move back
			mPosY -= (mVelY * timeStep);
			mCollider.y = mPosY;

		}
		mCollider.y = mPosY;
	}
#pragma endregion

#pragma region transfer_Right_Y
	for (unsigned int i = 0; i < transfer_Right.size(); i++)
	{

		SDL_Rect test2 = transfer_Right[i].getRect();

		//If the dot went too far up or down
		if (checkCollisionT_Right(mCollider, test2, transfer_Right))
		{
			//Move back
			mPosY -= (mVelY * timeStep);
			mCollider.y = mPosY;

		}
		mCollider.y = mPosY;
	}
#pragma endregion

}

Dot dot(60, 60);

void Dot::render()
{
	//Show the dot
	gDotTexture.render((float)mPosX, (float)mPosY);
}

void Wall::render()
{
	//Show the dot
	gWallTexture.render((int)mPosX, (int)mPosY);
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Zelda", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH - TILE_WIDTH - TILE_WIDTH, SCREEN_HEIGHT - TILE_HEIGHT - TILE_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load dot texture
	if (!gDotTexture.loadFromFile("dot.bmp"))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}

	return success;
}

void close()
{
	//Free loaded images
	gDotTexture.free();

	//Destroy window	
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

#pragma region Collisions
bool checkCollision(SDL_Rect a, SDL_Rect b, vector<Wall>Walls)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//for (unsigned int i = 0; i < Walls.size(); i++)
	//{
	//SDL_Rect test = Walls[i].getRect();
	//		Lwall = test.x;
	//		Rwall = test.x + test.w;
	//		Twall = test.y;
	//		Bwall = test.y + test.h;
	//If any of the sides from A are outside of B
	if (bottomA <= topB)
	{
		return false;
	}



	if (topA >= bottomB)
	{
		return false;
	}



	if (rightA <= leftB)
	{
		return false;
	}


	if (leftA >= rightB)
	{
		return false;
	}


	//}
	//If none of the sides from A are outside B
	return true;
}

bool checkCollisionT_Up(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Up)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	b = transfer_Up.front().getRect2();

	//Calculate the sides of rect B
	leftB = b.x;
	topB = b.y;

	b = transfer_Up.back().getRect2();
	rightB = b.x + b.w;
	bottomB = b.y + b.h;

	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{

		return false;
	}

	if (topA <= bottomB)
	{
		if (leftA >= leftB)
		{
			if (rightA <= rightB)
			{
				T_Direction = 1;
				new_level = true;
				dot.mPosY = 90;
			}
		}
	}

	if (rightA <= leftB)
	{
		return false;
	}

	if (leftA >= rightB)
	{
		return false;
	}

	//}
	//If none of the sides from A are outside B

	//T_Direction = 1;
	//a.x = SCREEN_HEIGHT - (TILE_WIDTH * 3);

	return true;
}

bool checkCollisionT_Down(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Down)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	b = transfer_Down.front().getRect2();

	//Calculate the sides of rect B
	leftB = b.x;
	topB = b.y;

	b = transfer_Down.back().getRect2();
	rightB = b.x + b.w;
	bottomB = b.y + b.h;

	if (bottomA >= topB)
	{
		if (leftA >= leftB)
		{
			if (rightA <= rightB)
			{
				T_Direction = 2;
				new_level = true;
				dot.mPosY = 90;
			}
		}
	}

	if (topA >= bottomB)
	{
		return false;
	}

	if (topA <= bottomB)
	{
		return false;
	}

	if (rightA <= leftB)
	{
		return false;
	}

	if (leftA >= rightB)
	{
		return false;
	}

	//}
	//If none of the sides from A are outside B

	//T_Direction = 1;
	//a.x = SCREEN_HEIGHT - (TILE_WIDTH * 3);

	return true;
}

bool checkCollisionT_Left(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Left)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	b = transfer_Left.front().getRect2();

	//Calculate the sides of rect B
	leftB = b.x;
	topB = b.y;

	b = transfer_Left.back().getRect2();
	rightB = b.x + b.w;
	bottomB = b.y + b.h;




	if (leftA <= rightB)
	{
		if (topA >= topB)
		{
			if (bottomA <= bottomB)
			{
				T_Direction = 3;
				new_level = true;
				dot.mPosX = 90;
			}
		}
	}

	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{
		return false;
	}


	if (rightA >= leftB)
	{
		return false;
	}

	//}
	//If none of the sides from A are outside B

	//T_Direction = 1;
	//a.x = SCREEN_HEIGHT - (TILE_WIDTH * 3);

	return true;
}

bool checkCollisionT_Right(SDL_Rect a, SDL_Rect b, vector<Wall>transfer_Right)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	int Lwall, Rwall, Twall, Bwall;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	b = transfer_Right.front().getRect2();

	//Calculate the sides of rect B
	leftB = b.x;
	topB = b.y;

	b = transfer_Right.back().getRect2();
	rightB = b.x + b.w;
	bottomB = b.y + b.h;

	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{
		return false;
	}



	if (rightA >= leftB)
	{
		if (topA >= topB)
		{
			if (bottomA <= bottomB)
			{
				T_Direction = 4;
				new_level = true;
				dot.mPosX = 90;
			}
		}
	}

	if (leftA <= rightB)
	{
		return false;
	}

	//}
	//If none of the sides from A are outside B

	//T_Direction = 1;
	//a.x = SCREEN_HEIGHT - (TILE_WIDTH * 3);

	return true;
}
#pragma endregion

vector<Wall> Walls;
vector<Wall>transfer_Up;
vector<Wall>transfer_Down;
vector<Wall>transfer_Left;
vector<Wall>transfer_Right;

void vector_clear()
{
	Walls.clear();
	transfer_Up.clear();
	transfer_Down.clear();
	transfer_Left.clear();
	transfer_Right.clear();
}

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;

			SDL_Rect test;

			//Keeps track of time between steps
			LTimer stepTimer;

			//While application is running
			while (!quit)
			{
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}

					//Handle input for the dot
					dot.handleEvent(e, new_level, currentlevel_x, currentlevel_y);
				}

				//Calculate time step
				float timeStep = stepTimer.getTicks() / 1000.f;

				//Move for time step
				dot.move(timeStep, Walls, transfer_Up, transfer_Down, transfer_Left, transfer_Right);


				//Restart step timer
				stepTimer.start();

				//Clear screen
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear(gRenderer);

				//Render wall
				SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
				//SDL_RenderDrawRect(gRenderer, &wall);



				dot.level_transfer(new_level, currentlevel_x, currentlevel_y);

				if (T_Direction == 1)
				{
					dot.mPosY = SCREEN_HEIGHT - (TILE_HEIGHT * 3);
					T_Direction = 0;
				}
				if (T_Direction == 2)
				{
					dot.mPosY = TILE_HEIGHT * 2;
					T_Direction = 0;
				}
				if (T_Direction == 3)
				{
					dot.mPosX = SCREEN_WIDTH - (TILE_WIDTH * 3);
					T_Direction = 0;
				}
				if (T_Direction == 4)
				{
					dot.mPosX = TILE_WIDTH * 2;
					T_Direction = 0;
				}
				if (new_level == true)
				{


#pragma region level_loader


					vector_clear();
					for (unsigned int i = 0; i < 12; i++)
					{
						for (unsigned int j = 0; j < 12; j++)
						{

							int tiletype;
							if ((currentlevel_x == 1) && (currentlevel_y == 1))
							{
								tiletype = tilemap1_1[i][j];
							}
							if ((currentlevel_x == 1) && (currentlevel_y == 2))
							{
								tiletype = tilemap1_2[i][j];
							}
							if ((currentlevel_x == 1) && (currentlevel_y == 3))
							{
								tiletype = tilemap1_3[i][j];
							}
							if ((currentlevel_x == 2) && (currentlevel_y == 1))
							{
								tiletype = tilemap2_1[i][j];
							}
							if ((currentlevel_x == 2) && (currentlevel_y == 2))
							{
								tiletype = tilemap2_2[i][j];
							}
							if ((currentlevel_x == 2) && (currentlevel_y == 3))
							{
								tiletype = tilemap2_3[i][j];
							}
							if ((currentlevel_x == 3) && (currentlevel_y == 1))
							{
								tiletype = tilemap3_1[i][j];
							}
							if ((currentlevel_x == 3) && (currentlevel_y == 2))
							{
								tiletype = tilemap3_2[i][j];
							}
							if ((currentlevel_x == 3) && (currentlevel_y == 3))
							{
								tiletype = tilemap3_3[i][j];
							}
							if ((currentlevel_x == 4) && (currentlevel_y == 2))
							{
								tiletype = tilemap4_2[i][j];
							}
							if ((currentlevel_x == 1) && (currentlevel_y == 1))
							{
								tiletype = tilemap1_1[i][j];
							}
							if ((currentlevel_x == 1) && (currentlevel_y == 1))
							{
								tiletype = tilemap1_1[i][j];
							}
							if ((currentlevel_x == 1) && (currentlevel_y == 1))
							{
								tiletype = tilemap1_1[i][j];
							}
							bool CC = false;

							//WORKING HERE
							//TRY TO AUTOMATE THE LEVEL LOADING CODE NEXT

							if (tiletype == 0)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 1)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 2)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 3)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 4)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 5)
							{
								CC = true;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 6)
							{
								CC = false;
								Wall wall1(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								Walls.push_back(wall1);
							}
							if (tiletype == 7)
							{
								CC = true;
								Wall wallT(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								transfer_Up.push_back(wallT);
							}
							if (tiletype == 8)
							{
								CC = true;
								Wall wallT(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								transfer_Down.push_back(wallT);
							}
							if (tiletype == 9)
							{
								CC = true;
								Wall wallT(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								transfer_Left.push_back(wallT);
							}
							if (tiletype == 10)
							{
								CC = true;
								Wall wallT(TILE_WIDTH * j - 40, TILE_HEIGHT * i - 40, TILE_WIDTH, TILE_HEIGHT, tiletype, CC);
								transfer_Right.push_back(wallT);
							}

						}
						// COLLISION FOR TRANSFER BLOCKS DOESN'T WORK BUT REST DO, SORT THIS OUT

					}




#pragma endregion


					new_level = false;
				};


#pragma region WallsVectorImages
				//------------------------------------------------------------
				for (unsigned int i = 0; i < Walls.size(); i++){


					SDL_Rect test = Walls[i].getRect2();
					if (Walls[i].getPicID() == 0)
					{
						gWallTexture.loadFromFile("black_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 1)
					{
						gWallTexture.loadFromFile("white_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 2)
					{
						gWallTexture.loadFromFile("blue_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 3)
					{
						gWallTexture.loadFromFile("brown_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 4)
					{
						gWallTexture.loadFromFile("green_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 5)
					{
						gWallTexture.loadFromFile("red_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 6)
					{
						gWallTexture.loadFromFile("yellow_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 7)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 8)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 9)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						Walls[i].render();
					}
					if (Walls[i].getPicID() == 10)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						Walls[i].render();
					}
					//SDL_RenderDrawRect(gRenderer, &test);
				}
#pragma endregion

#pragma region TransferVectorImages
				for (unsigned int i = 0; i < transfer_Up.size(); i++){


					SDL_Rect test = transfer_Up[i].getRect2();
					if (transfer_Up[i].getPicID() == 0)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 1)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 2)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 3)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 4)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 5)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 6)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 7)
					{
						gWallTexture.loadFromFile("tp_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 8)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 9)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					if (transfer_Up[i].getPicID() == 10)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Up[i].render();
					}
					//SDL_RenderDrawRect(gRenderer, &test);
				}
				for (unsigned int i = 0; i < transfer_Down.size(); i++){


					SDL_Rect test = transfer_Down[i].getRect2();
					if (transfer_Down[i].getPicID() == 0)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 1)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 2)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 3)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 4)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 5)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 6)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 7)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 8)
					{
						gWallTexture.loadFromFile("tp_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 9)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					if (transfer_Down[i].getPicID() == 10)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Down[i].render();
					}
					//SDL_RenderDrawRect(gRenderer, &test);
				}
				for (unsigned int i = 0; i < transfer_Left.size(); i++){


					SDL_Rect test = transfer_Left[i].getRect2();
					if (transfer_Left[i].getPicID() == 0)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 1)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 2)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 3)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 4)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 5)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 6)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 7)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 8)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 9)
					{
						gWallTexture.loadFromFile("tp_block.bmp");
						transfer_Left[i].render();
					}
					if (transfer_Left[i].getPicID() == 10)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Left[i].render();
					}
					//SDL_RenderDrawRect(gRenderer, &test);
				}
				for (unsigned int i = 0; i < transfer_Right.size(); i++){


					SDL_Rect test = transfer_Right[i].getRect2();
					if (transfer_Right[i].getPicID() == 0)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 1)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 2)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 3)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 4)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 5)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 6)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 7)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 8)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 9)
					{
						gWallTexture.loadFromFile("invis_block.bmp");
						transfer_Right[i].render();
					}
					if (transfer_Right[i].getPicID() == 10)
					{
						gWallTexture.loadFromFile("tp_block.bmp");
						transfer_Right[i].render();
					}
					//SDL_RenderDrawRect(gRenderer, &test);
				}
#pragma endregion

				//Render dot
				dot.render();

				//Update screen
				SDL_RenderPresent(gRenderer);


			}

		}


		//Free resources and close SDL
		close();

		return 0;
	}
}

